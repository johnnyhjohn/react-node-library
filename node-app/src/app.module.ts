import { Module } from '@nestjs/common';
import { RouterModule } from '@nestjs/core';

import { AppController } from './app.controller';
import { AppService } from './app.service';

import { BooksModule } from './app/domain/books/books.module';
import { CategoriesModule } from './app/domain/categories/categories.module';
import { PublishersModule } from './app/domain/publishers/publishers.module';
import { AuthorModule } from './app/domain/author/author.module';
import { UserModule } from './app/domain/user/user.module';
import { BookReviewModule } from './app/domain/bookReview/bookReview.module';
import { AuthModule } from './app/domain/auth/auth.module';

@Module({
  imports: [BooksModule, CategoriesModule, PublishersModule, AuthorModule, UserModule, BookReviewModule, AuthModule],
  controllers: [AppController],
  providers: [AppService]
})
export class AppModule {}
