import { Controller, Get, Post, UseGuards, Body, Headers, Req } from '@nestjs/common';
import { IResponse } from 'src/shared/domain/ApiResponse';
import { Request } from 'express';
import { IUserEntity } from '../user/user.entity';
import { AuthService } from 'src/app/providers/auth/auth.service';

@Controller('auth')
export class AuthController {
  	constructor(private readonly authService: AuthService) {}

  	@Post('login')
  	authenticate(@Body() userCredentials: IUserEntity ): IResponse {
        return this.authService.verifyCredentials( userCredentials );
  	}

  	@Post('register')
  	register(@Body() userCredentials: IUserEntity ): IResponse {
        return this.authService.createUser( userCredentials );
  	}

  	@Post('logout')
  	logout(@Req() request : Request): IResponse {
        return this.authService.logout(request.headers['access_token'] );
  	}
}

