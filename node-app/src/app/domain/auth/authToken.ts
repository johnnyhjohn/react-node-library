import AuthTokenModel from "src/shared/database/models/AuthToken";
import UserModel from "src/shared/database/models/User";
import { IUserEntity } from "../user/user.entity";
import UserRepository from "src/app/repository/user/user.repository";

export class AuthToken {
    public token : string;

    constructor() {
        this.token = '';
    }

    async getToken() {
        return AuthTokenModel.findOne({where : {token : this.token}});
    }

    async generate (id_user) {
        if (!id_user) {
          throw new Error('AuthToken requires a user ID')
        }
    
        const possibleCharacters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' + 'abcdefghijklmnopqrstuvwxyz0123456789';
    
        for (let i = 0; i < 15; i++) {
            this.token += possibleCharacters.charAt(
                Math.floor(Math.random() * possibleCharacters.length)
            );
        }

        const token = this.token;

        return AuthTokenModel.create({ token, id_user }, {include : [ {model : UserModel, as : 'Users'} ]})
    }

    async delete(accessToken : string | string[]) {
        return AuthTokenModel.destroy({ where: { token:accessToken } });
    }
}