import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { AuthMiddleware } from 'src/shared/middleware/auth.middleware';

@Injectable()
export class LocalAuthGuard implements CanActivate {
    async canActivate( context: ExecutionContext ): Promise<boolean> {
        const request = context.switchToHttp().getRequest();
   
        return new AuthMiddleware().validateRequest(request);
    }
}