import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthService } from 'src/app/providers/auth/auth.service';
import { IUserEntity } from '../user/user.entity';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(private authService: AuthService) {
    super();
  }

  async validate(username: string, password : string) {
    // console.log('userCredentials: ', userCredentials, teste);
    const userCredential = {
        username : username,
        password : password
    }
  }
  static async validate2(context: any) {
    console.log('userCredentials: ', context);
  }
}