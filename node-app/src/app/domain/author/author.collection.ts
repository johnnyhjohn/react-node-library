import { Entity } from 'src/shared/domain/Entity';
import { IAuthorEntity } from './author.entity';
import AuthorModel from 'src/shared/database/models/Author';
import { ICollection } from 'src/shared/collection/collection.entity';
import { Author } from './author';

export class AuthorCollection implements ICollection{

    private authors : IAuthorEntity[];

    constructor() {
        this.authors = [];
    }

    add( authorModel: AuthorModel ) {
        this.authors.push( Author.createFromEntity(authorModel) );
        return this;
    }

    get() : IAuthorEntity[] {
        return this.authors;
    }

    toList( authors: AuthorModel[] ) {
        this.authors = authors.map ( (author) => {
            return Author.createFromEntity(author);
        })
        return this;
    }
}