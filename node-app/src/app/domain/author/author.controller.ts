import { Controller, Get, Post, Put, Delete, Req, Body,Param } from '@nestjs/common';
import { Request } from 'express';
import { IResponse } from 'src/shared/domain/ApiResponse';
import { IAuthorEntity } from './author.entity';
import { AuthorService } from 'src/app/providers/author/author.service';

@Controller('author')
export class AuthorController {
  	constructor(private readonly authorService: AuthorService) {}

  	@Get()
  	getAuthors(@Req() request: Request) {
    	return this.authorService.getAll();
  	}

	@Get(':id')
	findOne(@Param('id') id: string) {
    	return this.authorService.get(id);
	}

  	@Post()
  	createAuthor(@Body() author: IAuthorEntity ): IResponse {
    	return this.authorService.create(author);
  	}

	@Put(':id')
	update(@Param('id') id: string, @Body() authorToEdit: IAuthorEntity) {
    	return this.authorService.edit(id, authorToEdit);
	}

	@Delete(':id')
	remove(@Param('id') id: string) {
    	return this.authorService.delete(id);
	}
}
