
export const RequiredFields = [
    'name',
]

export interface IAuthorEntity{
    id : string;
    name: string;
    biography?: string;
}
