import { Entity } from 'src/shared/domain/Entity';
import { IAuthorEntity, RequiredFields } from './author.entity';
import AuthorModel from 'src/shared/database/models/Author';

import { Validate } from '../validation/validate.factory';
import { IValidation } from '../validation/validation.entity';

export class Author extends Entity<IAuthorEntity>{
    public id : string;
    public name : string;
    public biography : string;

    constructor( props : IAuthorEntity ) {
        super(props);
    }

    static createFromEntity(authorModel : AuthorModel) : IAuthorEntity {
        return new Author(authorModel.toJSON()).getRecord();
    }

    validate() : IValidation {
        return new Validate().validateEntity(this.record, RequiredFields);
    }

    create() : AuthorModel {
        return AuthorModel.build(this.record);
    }

    update(id : string) {
        return AuthorModel.update(
            this.record, 
            {
                returning: true,
                where : {"id" : id }
            }
        );
    }

    static delete(id : string) {
        return AuthorModel.destroy(
            {
                where : {"id" : id }
            }            
        )
    }
}