import { Entity } from 'src/shared/domain/Entity';
import { IBookReviewEntity } from './bookReview.entity';
import BookReviewModel from 'src/shared/database/models/BookReview';
import { BookReview } from './bookReview';
import { ICollection } from 'src/shared/collection/collection.entity';

export class BookReviewCollection implements ICollection{

    private bookReview : IBookReviewEntity[];

    constructor() {
        this.bookReview = [];
    }

    add( bookModel: BookReviewModel ) {
        this.bookReview.push( BookReview.createFromEntity(bookModel) );
        return this;
    }

    get() : IBookReviewEntity[] {
        return this.bookReview;
    }

    toList( bookReviewRecords: BookReviewModel[] ) {
        this.bookReview = bookReviewRecords.map ( (bookReview) => {
            return BookReview.createFromEntity(bookReview);
        })
        return this;
    }
}