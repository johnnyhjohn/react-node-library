import { Controller, Get, Post, Put, Delete, Param, Req, Body } from '@nestjs/common';
import { Request } from 'express';
import { BookReviewService } from 'src/app/providers/bookReview/bookReview.service';
import { BookReview } from './bookReview';
import { IBookReviewEntity } from './bookReview.entity';
import { IResponse } from 'src/shared/domain/ApiResponse';

@Controller('book-review')
export class BookReviewController {
  	constructor(private readonly bookReviewService: BookReviewService) {}

  	@Get()
  	getBooks(@Req() request: Request) {
    	return this.bookReviewService.getAll();
  	}

	@Get(':id')
	findOne(@Param('id') id: string) {
		return this.bookReviewService.get(id);
	}

	@Post() 
	create(@Body() bookReview: IBookReviewEntity ): IResponse {
		return this.bookReviewService.create( bookReview );
	}

	@Put(':id')
	update(@Param('id') id: string, @Body() bookReviewToEdit: IBookReviewEntity) {
		return this.bookReviewService.edit( id, bookReviewToEdit );
	}

	@Delete(':id')
	remove(@Param('id') id: string) {
		return this.bookReviewService.delete( id );
	}
}