import { Model } from 'sequelize'
import { IUserEntity } from '../user/user.entity'
import { IBookEntity } from '../books/book.entity'

export const RequiredFields = [
    'title',
]

export interface IBookReviewEntity{
    id : string;
    title: string;
    review: string; 
    rating: number;
    user : IUserEntity;
    book : IBookEntity;
}