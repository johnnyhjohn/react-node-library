import { Module } from '@nestjs/common';
import { BookReviewController } from './bookReview.controller';
import { BookReviewService } from 'src/app/providers/bookReview/bookReview.service';

@Module({
  imports: [],
  controllers: [BookReviewController],
  providers: [BookReviewService],
})
export class BookReviewModule {}
