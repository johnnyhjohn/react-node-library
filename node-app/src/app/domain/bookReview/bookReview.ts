import { IValidation } from 'src/app/domain/validation/validation.entity';
import { Entity } from 'src/shared/domain/Entity';
import { IBookReviewEntity } from './bookReview.entity';

import { IUserEntity } from '../user/user.entity';
import { IBookEntity } from '../books/book.entity';
import { Book } from '../books/books';

import BookReviewModel from 'src/shared/database/models/BookReview';

import { ValidateBook } from '../validation/validateBook.factory';

export class BookReview extends Entity<IBookReviewEntity>{
    public id : string;
    public title : string;
    public review : string;
    public rating : number;
    public user: IUserEntity;
    public book: IBookEntity;

    constructor( props : IBookReviewEntity ) {
        super(props);
    }

    static createFromEntity(bookReviewModel : BookReviewModel) : IBookReviewEntity {
        return new BookReview( BookReview.transformModelToEntity( bookReviewModel ) ).getRecord();
    }

    static transformModelToEntity( bookReviewModel : BookReviewModel ) : IBookReviewEntity {
        const bookReviewFromModel = bookReviewModel.toJSON();
        return {
            id : bookReviewFromModel.id,
            title : bookReviewFromModel.title,
            review : bookReviewFromModel.review,
            rating : bookReviewFromModel.language,
            user : bookReviewFromModel.Users,
            book: Book.fromObject(bookReviewFromModel.Books),
        }
    }

    create() : BookReviewModel {
        return BookReviewModel.build(this.record);
    }

    update(id : string) {
        return BookReviewModel.update(
            this.record, 
            {
                returning: true,
                where : {"id" : id }
            }
        );
    }

    static delete(id : string) {
        return BookReviewModel.destroy(
            {
                where : {"id" : id }
            }            
        )
    }
}