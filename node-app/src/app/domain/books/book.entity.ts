import { Model } from 'sequelize'
import { ICategoryEntity } from '../categories/categories.entity'
import { IAuthorEntity } from '../author/author.entity'
import { IPublisherEntity } from '../publishers/publisher.entity'

export const RequiredFields = [
    'title',
]

export interface IBookEntity{
    id : string;
    title: string;
    description?: string;
    pages: number;
    language: string;
    author : IAuthorEntity;
    publisher : IPublisherEntity;
    category: ICategoryEntity;
}