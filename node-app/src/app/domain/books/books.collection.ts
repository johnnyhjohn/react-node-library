import { Entity } from 'src/shared/domain/Entity';
import { IBookEntity } from './book.entity';
import BookModel from 'src/shared/database/models/Book';
import { Book } from './books';
import { ICollection } from 'src/shared/collection/collection.entity';

export class BooksCollection implements ICollection{

    private books : IBookEntity[];

    constructor() {
        this.books = [];
    }

    add( bookModel: BookModel ) {
        this.books.push( Book.createFromEntity(bookModel) );
        return this;
    }

    get() : IBookEntity[] {
        return this.books;
    }

    toList( bookRecords: BookModel[] ) {
        this.books = bookRecords.map ( (book) => {
            return Book.createFromEntity(book);
        })
        return this;
    }
}