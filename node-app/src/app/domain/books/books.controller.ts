import { Controller, Get, Post, Put, Delete, Param, Req, Body } from '@nestjs/common';
import { Request } from 'express';
import { BooksService } from '../../providers/books/books.service';
import { Book } from './books';
import { IBookEntity } from './book.entity';
import { IResponse } from 'src/shared/domain/ApiResponse';

@Controller('books')
export class BooksController {
  	constructor(private readonly booksService: BooksService) {}

  	@Get()
  	getBooks(@Req() request: Request) {
    	return this.booksService.getAll();
  	}

	@Get(':id')
	findOne(@Param('id') id: string) {
		return this.booksService.get(id);
	}

	@Post()
	createCategories(@Body() category: IBookEntity ): IResponse {
		return this.booksService.create( category );
	}

	@Put(':id')
	update(@Param('id') id: string, @Body() categoryToEdit: IBookEntity) {
		return this.booksService.edit( id, categoryToEdit );
	}

	@Delete(':id')
	remove(@Param('id') id: string) {
		return this.booksService.delete( id );
	}
}