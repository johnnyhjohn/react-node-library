import { IValidation } from 'src/app/domain/validation/validation.entity';
import { Entity } from 'src/shared/domain/Entity';
import {IBookEntity} from './book.entity'

import { ICategoryEntity } from '../categories/categories.entity';
import { IAuthorEntity } from '../author/author.entity';
import { IPublisherEntity } from '../publishers/publisher.entity';

import BookModel from 'src/shared/database/models/Book';

import { ValidateBook } from '../validation/validateBook.factory';

export class Book extends Entity<IBookEntity>{
    public id : string;
    public title : string;
    public description : string;
    public language : string;
    public pages : number;
    public category: ICategoryEntity;
    public author: IAuthorEntity;
    public publisher: IPublisherEntity;

    constructor( props : IBookEntity ) {
        super(props);
    }

    static createFromEntity(bookModel : BookModel) : IBookEntity {
        return new Book( Book.transformModelToEntity( bookModel ) ).getRecord();
    }

    static transformModelToEntity( bookModel : BookModel ) : IBookEntity {
        const bookFromModel = bookModel.toJSON();
        return this.fromObject(bookFromModel);

    }

    static fromObject(object : any) : IBookEntity {
        return {
            id : object.id,
            title : object.title,
            description : object.description,
            language : object.language,
            pages : object.pages,
            category: object.Categories,
            author: object.Authors,
            publisher: object.Publishers,
        }
    } 

    create() : BookModel {
        return BookModel.build(this.record);
    }

    update(id : string) {
        return BookModel.update(
            this.record, 
            {
                returning: true,
                where : {"id" : id }
            }
        );
    }

    static delete(id : string) {
        return BookModel.destroy(
            {
                where : {"id" : id }
            }            
        )
    }
}