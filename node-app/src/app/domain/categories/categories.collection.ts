import { Entity } from 'src/shared/domain/Entity';
import { ICategoryEntity } from './categories.entity';
import CategoryModel from 'src/shared/database/models/Category';
import { Category } from './categories';
import { ICollection } from 'src/shared/collection/collection.entity';

export class CategoryCollection implements ICollection{

    private categories : ICategoryEntity[];

    constructor() {
        this.categories = [];
    }

    add( categoryModel: CategoryModel ) {
        this.categories.push( Category.createFromEntity(categoryModel) );
        return this;
    }

    get() : ICategoryEntity[] {
        return this.categories;
    }

    toList( categories: CategoryModel[] ) {
        this.categories = categories.map ( (category) => {
            return Category.createFromEntity(category);
        })
        return this;
    }
}