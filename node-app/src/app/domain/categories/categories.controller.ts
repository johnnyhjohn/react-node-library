import { Controller, Get, Post, Put, Delete, Req, Body,Param } from '@nestjs/common';
import { Request } from 'express';
import { ICategoryEntity } from './categories.entity';
import { IResponse } from 'src/shared/domain/ApiResponse';
import { CategoriesService } from '../../providers/categories/categories.service';
import { Category } from './categories';

@Controller('categories')
export class CategoriesController {
  	constructor(private readonly categoriesService: CategoriesService) {}

  	@Get()
  	getCategories(@Req() request: Request) {
    	return this.categoriesService.getAll();
  	}

	@Get(':id')
	findOne(@Param('id') id: string) {
    	return this.categoriesService.get(id);
	}

  	@Post()
  	createCategories(@Body() category: ICategoryEntity ): IResponse {
    	return this.categoriesService.create( category );
  	}

	@Put(':id')
	update(@Param('id') id: string, @Body() categoryToEdit: ICategoryEntity) {
    	return this.categoriesService.edit( id, categoryToEdit );
	}

	@Delete(':id')
	remove(@Param('id') id: string) {
    	return this.categoriesService.delete( id );
	}
}
