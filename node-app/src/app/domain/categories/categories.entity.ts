export const RequiredFields = [
    'name'
]

export interface ICategoryEntity{
    id : string;
    name: string;
    description?: string;
}
