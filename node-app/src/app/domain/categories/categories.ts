import { Entity } from 'src/shared/domain/Entity';
import { ICategoryEntity, RequiredFields } from './categories.entity';
import CategoryModel from 'src/shared/database/models/Category';
import { Validate } from '../validation/validate.factory';
import { IValidation } from '../validation/validation.entity';

export class Category extends Entity<ICategoryEntity>{
    public id : string;
    public name : string;
    public description : string;

    constructor( props : ICategoryEntity ) {
        super(props);
    }

    static createFromEntity(categoryModel : CategoryModel) : ICategoryEntity {
        return new Category(categoryModel.toJSON()).getRecord();
    }

    validate() : IValidation {
        return new Validate().validateEntity(this.record, RequiredFields);
    }

    create() : CategoryModel {
        return CategoryModel.build(this.record);
    }

    update(id : string) {
        return CategoryModel.update(
            this.record, 
            {
                returning: true,
                where : {"id" : id }
            }
        );
    }

    static delete(id : string) {
        return CategoryModel.destroy(
            {
                where : {"id" : id }
            }            
        )
    }
}