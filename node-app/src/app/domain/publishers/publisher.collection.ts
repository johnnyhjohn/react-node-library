import { ICollection } from "src/shared/collection/collection.entity";
import PublisherModel from "src/shared/database/models/Publisher";
import { Publisher } from "./publisher";
import { IPublisherEntity } from "./publisher.entity";

export class PubliserCollection implements ICollection{

    private publishers : IPublisherEntity[];

    constructor() {
        this.publishers = [];
    }

    add( publisherModel : PublisherModel ) {
        this.publishers.push( Publisher.createEntity(publisherModel) );
        return this;
    }

    toList(publishersModel: PublisherModel[]){
        this.publishers = publishersModel.map( publisher => Publisher.createEntity( publisher ) );
        return this;
    }

    get() : IPublisherEntity[] {
        return this.publishers;
    }
}