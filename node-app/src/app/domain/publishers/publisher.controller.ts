import { Controller, Get, Post, Put, Delete, Req, Body,Param } from '@nestjs/common';
import { Request } from 'express';
import { IPublisherEntity } from './publisher.entity';
import { IResponse } from 'src/shared/domain/ApiResponse';

import { PublisherService } from 'src/app/providers/publishers/publisher.service';

@Controller('publishers')
export class PublisherController {
  	constructor(private readonly publisherService: PublisherService) {}

    @Get()
    getAll() {
        return this.publisherService.getAll();
    }

    @Get(':id')
    get( @Param('id') id : string ) {
        return this.publisherService.get( id );
    }

    @Post()
    create(@Body() publisherRequest : IPublisherEntity) {
        return this.publisherService.create( publisherRequest );
    }

    @Put(':id')
	update(@Param('id') id: string, @Body() publisherRequest: IPublisherEntity) {
        return this.publisherService.update( id, publisherRequest );
    }
    
    @Delete(':id')
    delete(@Param('id') id : string) {
        return this.publisherService.delete( id );
    }
}
