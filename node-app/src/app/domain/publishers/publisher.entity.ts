export const RequiredFields = [
    'name'
]

export interface IPublisherEntity{
    id : string;
    name: string;
    website?: string;
}
