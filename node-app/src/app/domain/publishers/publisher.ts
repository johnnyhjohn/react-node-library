import { Entity } from "src/shared/domain/Entity";
import { IPublisherEntity } from "./publisher.entity";
import PublisherModel from "src/shared/database/models/Publisher";

export class Publisher extends Entity<IPublisherEntity>{

    public id : string;
    public name : string;
    public website : string;

    constructor( props : IPublisherEntity ) {
        super(props);
    }

    static createEntity(publisherModel : PublisherModel) : IPublisherEntity {
        return new Publisher(publisherModel.toJSON()).getRecord();
    }

    create() : PublisherModel{
        return PublisherModel.build( this.record );
    }

    async update( id : string ) {
        return PublisherModel.update(
            this.record,
            {
                returning: true,
                where : {"id" : id }
            }
        )
    }

    static async delete( id : string) {
        return PublisherModel.destroy({
            where : { "id" : id }
        })
    }
}

