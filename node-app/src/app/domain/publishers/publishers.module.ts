import { Module } from '@nestjs/common';

import { PublisherController } from './publisher.controller';
import { PublisherService } from 'src/app/providers/publishers/publisher.service';

@Module({
  imports: [],
  controllers: [PublisherController],
  providers: [PublisherService],
})
export class PublishersModule {}
