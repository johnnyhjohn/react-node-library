import { Entity } from 'src/shared/domain/Entity';
import { IUserEntity } from './user.entity';
import UserModel from 'src/shared/database/models/User';
import { ICollection } from 'src/shared/collection/collection.entity';
import { User } from './user';

export class UserCollection implements ICollection{

    private users : IUserEntity[];

    constructor() {
        this.users = [];
    }

    add( userModel: UserModel ) {
        this.users.push( User.createFromEntity(userModel) );
        return this;
    }

    get() : IUserEntity[] {
        return this.users;
    }

    toList( users: UserModel[] ) {
        this.users = users.map ( (user) => {
            return User.createFromEntity(user);
        })
        return this;
    }
}