import { Controller, Get, Post, Put, Delete, Req, Body,Param } from '@nestjs/common';
import { Request } from 'express';
import { IResponse } from 'src/shared/domain/ApiResponse';
import { IUserEntity } from './user.entity';
import { UserService } from 'src/app/providers/user/user.service';

@Controller('user')
export class UserController {
  	constructor(private readonly userService: UserService) {}

  	@Get()
  	getAuthors(@Req() request: Request) {
    	return this.userService.getAll();
  	}

	@Get(':id')
	findOne(@Param('id') id: string) {
    	return this.userService.get(id);
	}

  	@Post()
  	createUser(@Body() user: IUserEntity ): IResponse {
    	return this.userService.create(user);
  	}

	@Put(':id')
	update(@Param('id') id: string, @Body() userToEdit: IUserEntity) {
    	return this.userService.edit(id, userToEdit);
	}

	@Delete(':id')
	remove(@Param('id') id: string) {
    	return this.userService.delete(id);
	}
}
