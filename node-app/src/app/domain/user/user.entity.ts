
export const RequiredFields = [
    'username',
    'email',
    'password'
]

export interface IUserEntity{
    id : string;
    username: string;
    email: string;
    image: string; 
    password: string;
}
