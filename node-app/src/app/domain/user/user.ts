import * as bcrypt from 'bcrypt';

import { Entity } from 'src/shared/domain/Entity';
import { IUserEntity, RequiredFields } from './user.entity';
import UserModel from 'src/shared/database/models/User'; 

import { Validate } from '../validation/validate.factory';
import { IValidation } from '../validation/validation.entity';

export class User extends Entity<IUserEntity>{
    public id : string;
    public username : string;
    public email : string;
    public image : string;
    public password : string; 

    constructor( props : IUserEntity ) {
        super(props);
    }

    static createFromEntity(userModel : UserModel) : IUserEntity {
        return new User(userModel.toJSON()).getRecord();
    }

    validate() : IValidation {
        return new Validate().validateEntity(this.record, RequiredFields);
    }

    async create() {
        this.record.password = await bcrypt.hash(this.record.password, 10);
        return UserModel.build(this.record);
    }

    update(id : string) {
        return UserModel.update(
            this.record, 
            {
                returning: true,
                where : {"id" : id }
            }
        );
    }

    static delete(id : string) {
        return UserModel.destroy(
            {
                where : {"id" : id }
            }            
        )
    }
}