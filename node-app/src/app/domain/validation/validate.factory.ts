import {Validation} from './validation.factory'
import {IValidation, ValidationStatus} from './validation.entity'
import { IEntity } from 'src/shared/domain/Entity';

export class Validate extends Validation implements IValidation{

    private entity : IEntity;

    constructor() {
        super({message: "", status : ValidationStatus.ERROR, success: false})
    }

    public validateEntity(entity : IEntity, requiredFields : Array<string>) : IValidation {
        return this.validateRequiredFields( entity, requiredFields );
    }
}