import {Validation} from './validation.factory'
import {IValidation, ValidationStatus} from './validation.entity'
import { IBookEntity, RequiredFields } from 'src/app/domain/books/book.entity';

export class ValidateBook extends Validation implements IValidation{

    private bookEntity : IBookEntity;

    constructor() {
        super({message: "", status : ValidationStatus.ERROR, success: false})
    }

    public validateBook(bookEntity : IBookEntity) : IValidation {
        return this.validateRequiredFields( bookEntity, RequiredFields );
    }
}