export enum ValidationStatus {
    ERROR = 'error',
    SUCCESS = 'success'
}

export interface IValidation {
    status: ValidationStatus;
    message: string;
    success: boolean;
}