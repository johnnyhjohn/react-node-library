import { Entity, IEntity } from 'src/shared/domain/Entity';
import {IValidation, ValidationStatus} from './validation.entity'

export abstract class Validation implements IValidation{
    public message : string;
    public status : ValidationStatus;
    public success : boolean;

    constructor( props : IValidation ) {
        this.message = props.message;
        this.status = props.status;
        this.success = props.success;
    }

    public validateRequiredFields(entity : IEntity, requiredField : Array<string>) : IValidation {
        let isValid = this.isValid();
        let fieldsMissing = [];
        requiredField.map( field => {
            if( entity[field] == null || entity[field] == undefined ||  entity[field] == '') {
                isValid = false;
                fieldsMissing.push(field);
            }
        })

        return (fieldsMissing.length > 0)
            ? this.returnErrorMessage(`One or more field is missing. (${fieldsMissing.join(',')})`)
            : this.returnSuccessMessage();
    }

    public isValid() : Boolean {
        return this.status == ValidationStatus.SUCCESS;
    }

    public returnErrorMessage(message ?: string) : IValidation{
        this.message = (message) ? message : "Object invalid";
        this.status = ValidationStatus.ERROR;
        this.success = false;
        return this;
    }

    public returnSuccessMessage() : IValidation{
        this.message = "Object valid";
        this.status = ValidationStatus.SUCCESS;
        this.success = true;
        return this;
    }
}