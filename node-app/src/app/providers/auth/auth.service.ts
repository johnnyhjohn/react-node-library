import { Injectable } from '@nestjs/common';
import { IUserEntity } from 'src/app/domain/user/user.entity';
import { AuthToken } from 'src/app/domain/auth/authToken';
import UserRepository from 'src/app/repository/user/user.repository';
import { ApiResponse, IResponse } from 'src/shared/domain/ApiResponse';
import { UserService } from '../user/user.service';
import { User } from 'src/app/domain/user/user';

@Injectable()
export class AuthService {

    readonly ERROR_RETRIEVE_MESSAGE : string = 'Invalid username/password.';
    readonly SUCCESS_RETRIEVE_MESSAGE : string = 'User successfull authorized.';
    readonly SUCCESS_LOGOUT_MESSAGE : string = 'User successfull logged out.';
    readonly ERROR_MESSAGE : string = 'Unexpected error.';

    async verifyCredentials(userCredentials : IUserEntity) {
        try {
            const {username, password} = userCredentials;
            const user = await new UserRepository().getByCredentials(username, password);
            
            if(user == null) {
                throw new Error(this.ERROR_RETRIEVE_MESSAGE);
            }
            
            const authToken = await new AuthToken().generate(user.id);
    
            return new ApiResponse( {user, authToken}, this.SUCCESS_RETRIEVE_MESSAGE).returnSuccess();
        } catch(ex) {
            return new ApiResponse( {}, this.ERROR_RETRIEVE_MESSAGE ).returnError(ex.message);
        }
    }

    async createUser(userCredentials : IUserEntity) {
        try{
            const userFactory = new User(userCredentials);
            
            const validator = userFactory.validate();
            
            if( validator.success === false ) {
                throw new Error(validator.message);  
            }
            
            const userToCreate = await userFactory.create();
            await userToCreate.save();
            
            const user = User.createFromEntity(userToCreate);
            const authToken = await new AuthToken().generate(user.id);
    
            return new ApiResponse( {user, authToken}, this.SUCCESS_RETRIEVE_MESSAGE).returnSuccess();
        } catch(ex) {
            return new ApiResponse( {}, this.ERROR_RETRIEVE_MESSAGE ).returnError(ex.message);
        }
    }

    async logout(token : string | string[]) {
        try {
            const authToken = await new AuthToken().delete( token );
            if(authToken == 0) {
                throw new Error('No user to logout.');
            }
            return new ApiResponse( authToken, this.SUCCESS_LOGOUT_MESSAGE).returnSuccess();
        } catch(ex) {
            return new ApiResponse( {}, this.ERROR_MESSAGE ).returnError(ex.message);
        }
    }
}