import { Injectable } from '@nestjs/common';
import { IAuthorEntity } from 'src/app/domain/author/author.entity';
import { Author } from '../../domain/author/author';
import AuthorRepository from 'src/app/repository/author/author.repository';
import { ApiResponse, IResponse } from 'src/shared/domain/ApiResponse';

@Injectable()
export class AuthorService {

    private readonly SUCCESS_INSERT_MESSAGE :string = 'Author successful inserted.';
    private readonly SUCCESS_UPDATED_MESSAGE :string = 'Author successful updated.';
    private readonly SUCCESS_RETRIEVE_MESSAGE :string = 'Author successful retrieved';
    private readonly SUCCESS_DELETED_MESSAGE :string = 'Author successful deleted';
    private readonly ERROR_INSERT_MESSAGE :string = 'Error on insert Author';
    private readonly ERROR_UPDATE_MESSAGE :string = 'Error on UPDATE Author';
    private readonly ERROR_DELETE_MESSAGE :string = 'Error on DELETE Author';
    private readonly ERROR_RETRIEVE_MESSAGE :string = 'Error on retrieve Author';
    private readonly ERROR_INVALID_RECORD_MESSAGE :string = "Invalid record.";

    async getAll() {
        try {
            const authors = await new AuthorRepository().getAll();
            return new ApiResponse( authors, this.SUCCESS_RETRIEVE_MESSAGE).returnSuccess();
        } catch(ex) {
            return new ApiResponse( {}, this.ERROR_RETRIEVE_MESSAGE ).returnError(ex.message);
        }
    }

    async get(authorId : string) {
        try {
            const author = await new AuthorRepository().get(authorId);
            return new ApiResponse(author, this.SUCCESS_RETRIEVE_MESSAGE).returnSuccess();
        } catch(ex) {
            return new ApiResponse( {}, this.ERROR_RETRIEVE_MESSAGE ).returnError(ex.message);
        }
    }

    create(authorEntity : IAuthorEntity) : IResponse {
        try {
            const authorFactory = new Author(authorEntity);
            const validator = authorFactory.validate();

            if( validator.success === false ) {
                throw new Error(validator.message);  
            }

            const authorToCreate = authorFactory.create();
            authorToCreate.save();

            return new ApiResponse( authorToCreate.toJSON(), this.SUCCESS_INSERT_MESSAGE ).returnSuccess();
        } catch(ex) {
            return new ApiResponse( {}, this.ERROR_INSERT_MESSAGE ).returnError(ex.message);
        }
    }

    async edit(id : string, authorEntity : IAuthorEntity) {
        try {
            const authorToCreate = await new Author(authorEntity).update(id);
            console.log(authorToCreate);
            if( authorToCreate == undefined || authorToCreate[0] == 0 ) {
                throw new Error(this.ERROR_INVALID_RECORD_MESSAGE);
            }
            
            return new ApiResponse( authorToCreate[1], this.SUCCESS_UPDATED_MESSAGE ).returnSuccess();
        } catch(ex) {
            return new ApiResponse( {}, this.ERROR_UPDATE_MESSAGE ).returnError(ex.message);
        }
    }

    async delete(authorId : string) {
        try {
            const authorToDelete = await Author.delete(authorId);

            if( authorToDelete == undefined || authorToDelete == 0 ) {
                throw new Error(this.ERROR_INVALID_RECORD_MESSAGE);
            }
            return new ApiResponse( authorToDelete, this.SUCCESS_DELETED_MESSAGE ).returnSuccess();
        } catch(ex) {
            return new ApiResponse( {}, this.ERROR_DELETE_MESSAGE ).returnError(ex.message);
        }
    }
}
