import { Injectable } from '@nestjs/common';
import { IBookReviewEntity } from 'src/app/domain/bookReview/bookReview.entity';
import { BookReviewCollection } from 'src/app/domain/bookReview/bookReview.collection';
import { BookReview } from 'src/app/domain/bookReview/bookReview';
import BookReviewRepository from 'src/app/repository/bookReview/bookReview.repository';
import { ApiResponse, IResponse } from 'src/shared/domain/ApiResponse';


@Injectable()
export class BookReviewService {
  	
    private readonly SUCCESS_INSERT_MESSAGE :string = 'Book Review successful inserted.';
    private readonly SUCCESS_UPDATED_MESSAGE :string = 'Book Review successful updated.';
    private readonly SUCCESS_RETRIEVE_MESSAGE :string = 'Book Reviews successful retrieved';
    private readonly SUCCESS_DELETED_MESSAGE :string = 'Book Reviews successful deleted';
    private readonly ERROR_INSERT_MESSAGE :string = 'Error on insert Book Review';
    private readonly ERROR_UPDATE_MESSAGE :string = 'Error on UPDATE Book Review';
    private readonly ERROR_DELETE_MESSAGE :string = 'Error on DELETE Book Review';
    private readonly ERROR_RETRIEVE_MESSAGE :string = 'Error on retrieve Book Reviews';
    private readonly ERROR_INVALID_RECORD_MESSAGE :string = "Invalid record.";

    async getAll() {
        try {
            const bookReviews = await new BookReviewRepository().getAll();
            return new ApiResponse( bookReviews, this.SUCCESS_RETRIEVE_MESSAGE).returnSuccess();
        } catch(ex) {
            return new ApiResponse( {}, this.ERROR_RETRIEVE_MESSAGE ).returnError(ex.message);
        }
    }

    async get(bookReviewId : string) {
        try {
            const bookReview = await new BookReviewRepository().get(bookReviewId);
            return new ApiResponse( bookReview, this.SUCCESS_RETRIEVE_MESSAGE).returnSuccess();
        } catch(ex) {
            return new ApiResponse( {}, this.ERROR_RETRIEVE_MESSAGE ).returnError(ex.message);
        }
    }

    create(bookReviewEntity : IBookReviewEntity) : IResponse {
        try {
            const bookReviewToCreate = new BookReview(bookReviewEntity).create();
            bookReviewToCreate.save();
            return new ApiResponse( bookReviewToCreate.toJSON(), this.SUCCESS_INSERT_MESSAGE ).returnSuccess();
        } catch(ex) {
            return new ApiResponse( {}, this.ERROR_INSERT_MESSAGE ).returnError(ex.message);
        }
    }

    async edit(id : string, bookReviewEntity : IBookReviewEntity) {
        try {
            const bookReviewToCreate = await new BookReview(bookReviewEntity).update(id);
            if( bookReviewToCreate == undefined || bookReviewToCreate[0] == 0 ) {
                throw new Error(this.ERROR_INVALID_RECORD_MESSAGE);
            }
            
            return new ApiResponse( bookReviewToCreate[1], this.SUCCESS_UPDATED_MESSAGE ).returnSuccess();
        } catch(ex) {
            return new ApiResponse( {}, this.ERROR_UPDATE_MESSAGE ).returnError(ex.message);
        }
    }

    async delete(bookReviewId : string) {
        try {
            const bookReviewToDelete = await BookReview.delete(bookReviewId);

            if( bookReviewToDelete == undefined || bookReviewToDelete == 0 ) {
                throw new Error(this.ERROR_INVALID_RECORD_MESSAGE);
            }
            return new ApiResponse( bookReviewToDelete, this.SUCCESS_DELETED_MESSAGE ).returnSuccess();
        } catch(ex) {
            return new ApiResponse( {}, this.ERROR_DELETE_MESSAGE ).returnError(ex.message);
        }
    }
}
