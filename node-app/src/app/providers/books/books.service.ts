import { Injectable } from '@nestjs/common';
import { IBookEntity } from 'src/app/domain/books/book.entity';
import { BooksCollection } from 'src/app/domain/books/books.collection';
import { Book } from '../../domain/books/books';
import BookRepository from 'src/app/repository/book/book.repository';
import { ApiResponse, IResponse } from 'src/shared/domain/ApiResponse';


@Injectable()
export class BooksService {
  	
    private readonly SUCCESS_INSERT_MESSAGE :string = 'Book successful inserted.';
    private readonly SUCCESS_UPDATED_MESSAGE :string = 'Book successful updated.';
    private readonly SUCCESS_RETRIEVE_MESSAGE :string = 'Books successful retrieved';
    private readonly SUCCESS_DELETED_MESSAGE :string = 'Books successful deleted';
    private readonly ERROR_INSERT_MESSAGE :string = 'Error on insert Book';
    private readonly ERROR_UPDATE_MESSAGE :string = 'Error on UPDATE Book';
    private readonly ERROR_DELETE_MESSAGE :string = 'Error on DELETE Book';
    private readonly ERROR_RETRIEVE_MESSAGE :string = 'Error on retrieve Books';
    private readonly ERROR_INVALID_RECORD_MESSAGE :string = "Invalid record.";

    async getAll() {
        try {
            const books = await new BookRepository().getAll();
            return new ApiResponse( books, this.SUCCESS_RETRIEVE_MESSAGE).returnSuccess();
        } catch(ex) {
            return new ApiResponse( {}, this.ERROR_RETRIEVE_MESSAGE ).returnError(ex.message);
        }
    }

    async get(categoryId : string) {
        try {
            const book = await new BookRepository().get(categoryId);
            return new ApiResponse( book, this.SUCCESS_RETRIEVE_MESSAGE).returnSuccess();
        } catch(ex) {
            return new ApiResponse( {}, this.ERROR_RETRIEVE_MESSAGE ).returnError(ex.message);
        }
    }

    create(bookEntity : IBookEntity) : IResponse {
        try {
            const bookToCreate = new Book(bookEntity).create();
            bookToCreate.save();
            return new ApiResponse( bookToCreate.toJSON(), this.SUCCESS_INSERT_MESSAGE ).returnSuccess();
        } catch(ex) {
            return new ApiResponse( {}, this.ERROR_INSERT_MESSAGE ).returnError(ex.message);
        }
    }

    async edit(id : string, bookEntity : IBookEntity) {
        try {
            const bookToCreate = await new Book(bookEntity).update(id);
            if( bookToCreate == undefined || bookToCreate[0] == 0 ) {
                throw new Error(this.ERROR_INVALID_RECORD_MESSAGE);
            }
            
            return new ApiResponse( bookToCreate[1], this.SUCCESS_UPDATED_MESSAGE ).returnSuccess();
        } catch(ex) {
            return new ApiResponse( {}, this.ERROR_UPDATE_MESSAGE ).returnError(ex.message);
        }
    }

    async delete(categoryId : string) {
        try {
            const bookToDelete = await Book.delete(categoryId);

            if( bookToDelete == undefined || bookToDelete == 0 ) {
                throw new Error(this.ERROR_INVALID_RECORD_MESSAGE);
            }
            return new ApiResponse( bookToDelete, this.SUCCESS_DELETED_MESSAGE ).returnSuccess();
        } catch(ex) {
            return new ApiResponse( {}, this.ERROR_DELETE_MESSAGE ).returnError(ex.message);
        }
    }
}
