import { Injectable } from '@nestjs/common';
import { ICategoryEntity, RequiredFields } from '../../domain/categories/categories.entity';
import { Category } from '../../domain/categories/categories';
import CategoryRepository from 'src/app/repository/categories/categories.repository';
import { ApiResponse, IResponse } from 'src/shared/domain/ApiResponse';

@Injectable()
export class CategoriesService {

    private readonly SUCCESS_INSERT_MESSAGE :string = 'Category successful inserted.';
    private readonly SUCCESS_UPDATED_MESSAGE :string = 'Category successful updated.';
    private readonly SUCCESS_RETRIEVE_MESSAGE :string = 'Categories successful retrieved';
    private readonly SUCCESS_DELETED_MESSAGE :string = 'Categories successful deleted';
    private readonly ERROR_INSERT_MESSAGE :string = 'Error on insert Category';
    private readonly ERROR_UPDATE_MESSAGE :string = 'Error on UPDATE Category';
    private readonly ERROR_DELETE_MESSAGE :string = 'Error on DELETE Category';
    private readonly ERROR_RETRIEVE_MESSAGE :string = 'Error on retrieve Categories';
    private readonly ERROR_INVALID_RECORD_MESSAGE :string = "Invalid record.";

    async getAll() {
        try {
            const categories = await new CategoryRepository().getAll();
            return new ApiResponse( categories, this.SUCCESS_RETRIEVE_MESSAGE).returnSuccess();
        } catch(ex) {
            return new ApiResponse( {}, this.ERROR_RETRIEVE_MESSAGE ).returnError(ex.message);
        }
    }

    async get(categoryId : string) {
        try {
            const category = await new CategoryRepository().get(categoryId);
            return new ApiResponse( category, this.SUCCESS_RETRIEVE_MESSAGE).returnSuccess();
        } catch(ex) {
            return new ApiResponse( {}, this.ERROR_RETRIEVE_MESSAGE ).returnError(ex.message);
        }
    }

    create(categoryEntity : ICategoryEntity) : IResponse {
        try {
            const categoryFactory = new Category(categoryEntity);
            const validator = categoryFactory.validate();

            if( validator.success === false ) {
                throw new Error(validator.message);  
            }

            const categoryToCreate = categoryFactory.create();
            categoryToCreate.save();

            return new ApiResponse( categoryToCreate.toJSON(), this.SUCCESS_INSERT_MESSAGE ).returnSuccess();
        } catch(ex) {
            return new ApiResponse( {}, this.ERROR_INSERT_MESSAGE ).returnError(ex.message);
        }
    }

    async edit(id : string, categoryEntity : ICategoryEntity) {
        try {
            const categoryToCreate = await new Category(categoryEntity).update(id);
            console.log(categoryToCreate);
            if( categoryToCreate == undefined || categoryToCreate[0] == 0 ) {
                throw new Error(this.ERROR_INVALID_RECORD_MESSAGE);
            }
            
            return new ApiResponse( categoryToCreate[1], this.SUCCESS_UPDATED_MESSAGE ).returnSuccess();
        } catch(ex) {
            return new ApiResponse( {}, this.ERROR_UPDATE_MESSAGE ).returnError(ex.message);
        }
    }

    async delete(categoryId : string) {
        try {
            const categoryToDelete = await Category.delete(categoryId);

            if( categoryToDelete == undefined || categoryToDelete == 0 ) {
                throw new Error(this.ERROR_INVALID_RECORD_MESSAGE);
            }
            return new ApiResponse( categoryToDelete, this.SUCCESS_DELETED_MESSAGE ).returnSuccess();
        } catch(ex) {
            return new ApiResponse( {}, this.ERROR_DELETE_MESSAGE ).returnError(ex.message);
        }
    }
}
