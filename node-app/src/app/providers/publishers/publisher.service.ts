import { Injectable, Put } from '@nestjs/common';
import { IPublisherEntity } from 'src/app/domain/publishers/publisher.entity';
import { ApiResponse, IResponse } from 'src/shared/domain/ApiResponse';
import PublisherRepository from 'src/app/repository/publishers/publisher.repository';
import { Publisher } from 'src/app/domain/publishers/publisher';


@Injectable()
export class PublisherService {

    private readonly SUCCESS_INSERT_MESSAGE :string = 'Publisher successful inserted.';
    private readonly SUCCESS_UPDATED_MESSAGE :string = 'Publisher successful updated.';
    private readonly SUCCESS_RETRIEVE_MESSAGE :string = 'Publishers successful retrieved';
    private readonly SUCCESS_DELETED_MESSAGE :string = 'Publishers successful deleted';
    private readonly ERROR_INSERT_MESSAGE :string = 'Error on insert Publisher';
    private readonly ERROR_UPDATE_MESSAGE :string = 'Error on UPDATE Publisher';
    private readonly ERROR_DELETE_MESSAGE :string = 'Error on DELETE Publisher';
    private readonly ERROR_RETRIEVE_MESSAGE :string = 'Error on retrieve Publishers';
    private readonly ERROR_INVALID_RECORD_MESSAGE :string = "Invalid record.";


    async getAll () {
        try{
            const publishers = await new PublisherRepository().getAll();
            return new ApiResponse(publishers, this.SUCCESS_RETRIEVE_MESSAGE).returnSuccess();
        } catch(ex) {
            return new ApiResponse({}, this.ERROR_RETRIEVE_MESSAGE).returnError( ex.message );
        }
    }

    async get(id : string) {
        try{
            console.log('id : ', id);
            const publishers = await new PublisherRepository().get(id);
            return new ApiResponse(publishers, this.SUCCESS_RETRIEVE_MESSAGE).returnSuccess();
        } catch(ex) {
            return new ApiResponse({}, this.ERROR_RETRIEVE_MESSAGE).returnError( ex.message );
        }
    }

    create( publisherToInsert : IPublisherEntity ) {
        try{
            const publisher = new Publisher(publisherToInsert).create();
            publisher.save();
            return new ApiResponse(publisher, this.SUCCESS_INSERT_MESSAGE).returnSuccess();
        } catch(ex) {
            return new ApiResponse({}, this.ERROR_INSERT_MESSAGE).returnError( ex.message );
        }
    }

    async update(id : string, publisherToUpdate : IPublisherEntity) {
        try{
            const publishersUpdated = await new Publisher(publisherToUpdate).update( id );

            if(publishersUpdated == undefined || publishersUpdated[0] == 0) {
                throw new Error( this.ERROR_INVALID_RECORD_MESSAGE );
            }

            return new ApiResponse(publishersUpdated[1], this.SUCCESS_UPDATED_MESSAGE).returnSuccess();
        } catch(ex) {
            return new ApiResponse({}, this.ERROR_UPDATE_MESSAGE).returnError( ex.message );
        }
    }

    async delete( id : string ) {
        try {
            const deletedPublisher = await Publisher.delete( id );

            if(deletedPublisher == undefined || deletedPublisher == 0) {
                throw new Error( this.ERROR_INVALID_RECORD_MESSAGE );
            }

            return new ApiResponse(deletedPublisher, this.SUCCESS_DELETED_MESSAGE).returnSuccess();
        } catch(ex) {
            return new ApiResponse({}, this.ERROR_DELETE_MESSAGE).returnError( ex.message );
        }
    }
}
