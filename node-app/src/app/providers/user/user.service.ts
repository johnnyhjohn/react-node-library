import { Injectable } from '@nestjs/common';
import { IUserEntity } from 'src/app/domain/user/user.entity';
import { User } from 'src/app/domain/user/user';
import UserRepository from 'src/app/repository/user/user.repository';
import { ApiResponse, IResponse } from 'src/shared/domain/ApiResponse';

@Injectable()
export class UserService {

    private readonly SUCCESS_INSERT_MESSAGE :string = 'User successful inserted.';
    private readonly SUCCESS_UPDATED_MESSAGE :string = 'User successful updated.';
    private readonly SUCCESS_RETRIEVE_MESSAGE :string = 'User successful retrieved';
    private readonly SUCCESS_DELETED_MESSAGE :string = 'User successful deleted';
    private readonly ERROR_INSERT_MESSAGE :string = 'Error on insert User';
    private readonly ERROR_UPDATE_MESSAGE :string = 'Error on UPDATE User';
    private readonly ERROR_DELETE_MESSAGE :string = 'Error on DELETE User';
    private readonly ERROR_RETRIEVE_MESSAGE :string = 'Error on retrieve User';
    private readonly ERROR_INVALID_RECORD_MESSAGE :string = "Invalid record.";

    async getAll() {
        try {
            const users = await new UserRepository().getAll();
            return new ApiResponse( users, this.SUCCESS_RETRIEVE_MESSAGE).returnSuccess();
        } catch(ex) {
            return new ApiResponse( {}, this.ERROR_RETRIEVE_MESSAGE ).returnError(ex.message);
        }
    }

    async get(userId : string) {
        try {
            const user = await new UserRepository().get(userId);
            return new ApiResponse(user, this.SUCCESS_RETRIEVE_MESSAGE).returnSuccess();
        } catch(ex) {
            return new ApiResponse( {}, this.ERROR_RETRIEVE_MESSAGE ).returnError(ex.message);
        }
    }

    async create(userEntity : IUserEntity) {
        try {
            const userFactory = new User(userEntity);
            const validator = userFactory.validate();
            
            if( validator.success === false ) {
                throw new Error(validator.message);  
            }
            
            const userToCreate = await userFactory.create();
            userToCreate.save();

            return new ApiResponse( userToCreate.toJSON(), this.SUCCESS_INSERT_MESSAGE ).returnSuccess();
        } catch(ex) {
            return new ApiResponse( {}, this.ERROR_INSERT_MESSAGE ).returnError(ex.message);
        }
    }

    async edit(id : string, userEntity : IUserEntity) {
        try {
            const userToCreate = await new User(userEntity).update(id);
            if( userToCreate == undefined || userToCreate[0] == 0 ) {
                throw new Error(this.ERROR_INVALID_RECORD_MESSAGE);
            }
            
            return new ApiResponse( userToCreate[1], this.SUCCESS_UPDATED_MESSAGE ).returnSuccess();
        } catch(ex) {
            return new ApiResponse( {}, this.ERROR_UPDATE_MESSAGE ).returnError(ex.message);
        }
    }

    async delete(userId : string) {
        try {
            const userToDelete = await User.delete(userId);

            if( userToDelete == undefined || userToDelete == 0 ) {
                throw new Error(this.ERROR_INVALID_RECORD_MESSAGE);
            }
            return new ApiResponse( userToDelete, this.SUCCESS_DELETED_MESSAGE ).returnSuccess();
        } catch(ex) {
            return new ApiResponse( {}, this.ERROR_DELETE_MESSAGE ).returnError(ex.message);
        }
    }
}
