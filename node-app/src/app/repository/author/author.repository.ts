import AuthorModel from "src/shared/database/models/Author";

import { AuthorCollection } from "src/app/domain/author/author.collection";
import { IAuthorEntity } from "src/app/domain/author/author.entity";

export default class AuthorRepository {
    
    private collection : AuthorCollection;

    constructor() {
        this.collection = new AuthorCollection()
    }

    async getAll() : Promise<IAuthorEntity[]> {
        const authors = await AuthorModel.findAll();
        return this.collection.toList(authors).get();
    }

    async get(id : string) : Promise<IAuthorEntity[]> {
        const authorById = await AuthorModel.findOne({
            where: {
              id: id
            }
        });
        
        return this.collection.add(authorById).get();
    }
}