import { BooksCollection } from "src/app/domain/books/books.collection";
import { IBookEntity } from "src/app/domain/books/book.entity";
import BookModel from "src/shared/database/models/Book";

import AuthorModel from "src/shared/database/models/Author";
import PublisherModel from "src/shared/database/models/Publisher";
import CategoryModel from "src/shared/database/models/Category";

export default class BookRepository {
    
    private collection : BooksCollection;

    constructor() {
        this.collection = new BooksCollection()
    }

    async getAll() : Promise<IBookEntity[]> {
        const categories = await BookModel.findAll({
            include: [
                {model: AuthorModel, as: AuthorModel.tableName},
                {model: PublisherModel, as: PublisherModel.tableName},
                {model: CategoryModel, as: CategoryModel.tableName}
            ]
        });
        
        return this.collection.toList(categories).get();
    }

    async get(categoryId : string) : Promise<IBookEntity[]> {
        const categoryById = await BookModel.findOne({
            where: {
              id: categoryId
            }
        });
        
        return this.collection.add(categoryById).get();
    }
}