import { BookReviewCollection } from "src/app/domain/bookReview/bookReview.collection";
import { IBookReviewEntity } from "src/app/domain/bookReview/bookReview.entity";
import BookReviewModel from "src/shared/database/models/BookReview";

import BookModel from "src/shared/database/models/Book";
import UserModel from "src/shared/database/models/User";
import AuthorModel from "src/shared/database/models/Author";
import CategoryModel from "src/shared/database/models/Category";
import PublisherModel from "src/shared/database/models/Publisher";

export default class BookReviewRepository {
    
    private collection : BookReviewCollection;

    constructor() {
        this.collection = new BookReviewCollection()
    }

    async getAll() : Promise<IBookReviewEntity[]> {
        const bookReview = await BookReviewModel.findAll({
            include: [
                {model: BookModel, as: BookModel.tableName, include : [ 
                    {model: AuthorModel, as: AuthorModel.tableName},
                    {model: PublisherModel, as: PublisherModel.tableName},
                    {model: CategoryModel, as: CategoryModel.tableName}] },
                {model: UserModel, as: UserModel.tableName, attributes : ["id", "username", "image"]},
            ]
        });
        
        return this.collection.toList(bookReview).get();
    }

    async get(bookReviewId : string) : Promise<IBookReviewEntity[]> {     
        const bookReviewById = await BookReviewModel.findOne({
            where: {
              id: bookReviewId
            },
            include: [
                {model: BookModel, as: BookModel.tableName, include : [ 
                    {model: AuthorModel, as: AuthorModel.tableName},
                    {model: PublisherModel, as: PublisherModel.tableName},
                    {model: CategoryModel, as: CategoryModel.tableName}] },
                {model: UserModel, as: UserModel.tableName, attributes : ["id", "username", "image"]},
            ]
        });
        
        return this.collection.add(bookReviewById).get();
    }
}