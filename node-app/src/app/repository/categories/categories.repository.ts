import { CategoryCollection } from "src/app/domain/categories/categories.collection";
import { ICategoryEntity } from "src/app/domain/categories/categories.entity";
import CategoryModel from "src/shared/database/models/Category";

export default class CategoryRepository {
    
    private collection : CategoryCollection;

    constructor() {
        this.collection = new CategoryCollection()
    }

    async getAll() : Promise<ICategoryEntity[]> {
        const categories = await CategoryModel.findAll();
        return this.collection.toList(categories).get();
    }

    async get(categoryId : string) : Promise<ICategoryEntity[]> {
        const categoryById = await CategoryModel.findOne({
            where: {
              id: categoryId
            }
        });
        
        return this.collection.add(categoryById).get();
    }
}