import PublisherModel from "src/shared/database/models/Publisher";

import { PubliserCollection } from "src/app/domain/publishers/publisher.collection";
import { IPublisherEntity } from "src/app/domain/publishers/publisher.entity";

export default class PublisherRepository {
    
    private collection : PubliserCollection;

    constructor() {
        this.collection = new PubliserCollection()
    }

    async getAll() : Promise<IPublisherEntity[]> {
        const publishers = await PublisherModel.findAll();
        return this.collection.toList(publishers).get();
    }

    async get(id : string) : Promise<IPublisherEntity[]> {
        const publisherById = await PublisherModel.findOne({
            where: {
              id: id
            }
        });
        
        return this.collection.add(publisherById).get();
    }
}