import * as bcrypt from 'bcrypt';
import UserModel from "src/shared/database/models/User";

import { UserCollection } from "src/app/domain/user/user.collection";
import { IUserEntity } from "src/app/domain/user/user.entity";
import { User } from 'src/app/domain/user/user';

export default class UserRepository {
    
    private collection : UserCollection;

    constructor() {
        this.collection = new UserCollection()
    }

    async getAll() : Promise<IUserEntity[]> {
        const users = await UserModel.findAll();
        return this.collection.toList(users).get();
    }

    async get(id : string) : Promise<IUserEntity[]> {
        const userById = await UserModel.findOne({
            where: {
              id: id
            }
        });
        
        return this.collection.add(userById).get();
    }

    async getByCredentials(username : string, password : string) : Promise<IUserEntity> {
        const userByUsername = await UserModel.findOne({
            where: {
              username: username
            }
        });

        if (userByUsername == null) return null;

        const user = User.createFromEntity(userByUsername);
        const isMatch = await bcrypt.compare(password, user.password );

        delete user.password;
        
        return ( isMatch ) ? user : null;

    }
}