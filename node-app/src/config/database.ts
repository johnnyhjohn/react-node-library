import { Dialect, Sequelize } from 'sequelize'

const sequelizeConnection = new Sequelize("postgres://zqaspyjqvnqget:9f9894b9a3e76f7730649ad57551ac7545175c0299ee5d9fe9ba1cdde9aa2ca4@ec2-34-236-87-247.compute-1.amazonaws.com:5432/d9upnhhqdc5c00", {
	dialectOptions: {
		ssl: {
			require: true,
			rejectUnauthorized: false
		}
	}
}
)

export default sequelizeConnection