import Category from "src/shared/database/models/Category"
import Publisher from "src/shared/database/models/Publisher"
import Author from "src/shared/database/models/Author"
import Book from "src/shared/database/models/Book"
import User from "src/shared/database/models/User"
import BookReview from "src/shared/database/models/BookReview"
import AuthToken from "src/shared/database/models/AuthToken"

const isDev = process.env.NODE_ENV === 'development'

const dbInit = () => Promise.all([
    Category.sync(),
    Publisher.sync(),
    Author.sync(),
    Book.sync(),
    User.sync(),
    AuthToken.sync(),
    BookReview.sync()
])

export default dbInit 