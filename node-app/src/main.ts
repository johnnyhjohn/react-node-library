import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import dbInit from './config/init'
import {LocalAuthGuard} from './app/domain/auth/local-auth.guard'

dbInit();

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true });
  app.useGlobalGuards(new LocalAuthGuard());
  await app.listen(3005);
}

bootstrap();
