import { Model } from "sequelize/dist";
import { IEntity } from "../domain/Entity";

export interface ICollection {
    add( model : Model ) : ICollection;
    toList(entity: Model[]) : ICollection;
    get() : IEntity[];
}