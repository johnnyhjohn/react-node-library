import { Sequelize, DataTypes, Model, STRING } from "sequelize";
import sequelizeConnection from '../../../config/database'

import User from "./User";

class AuthToken extends Model{}

AuthToken.init({
    id : {
        type: DataTypes.UUID,
        defaultValue : DataTypes.UUIDV4,
        allowNull: false,
        primaryKey: true
    },
    token: {
        type : DataTypes.STRING,
        allowNull: false
    },
    id_user : {
        type : DataTypes.UUID,
        allowNull : false,
        references : {
            model : 'Users',
            key : 'id'
        }
    }
}, {
    // Other model options go here
    sequelize: sequelizeConnection, // We need to choose the model name
    tableName: 'AuthTokens', // We need to pass the connection instance
    timestamps: false,
    schema: 'public'
});

AuthToken.belongsTo(User, { foreignKey: 'id_user', targetKey: 'id', as: 'Users' })
User.hasMany(AuthToken, { as : 'Users'});

export default AuthToken;