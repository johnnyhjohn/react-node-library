import { Sequelize, DataTypes, Model } from "sequelize";
import sequelizeConnection from '../../../config/database'

class Author extends Model{}

Author.init({
        id : {
            type: DataTypes.UUID,
            defaultValue : DataTypes.UUIDV4,
            allowNull: false,
            primaryKey: true
        },
        name : {
            type: DataTypes.STRING( 255 ),
            allowNull : false
        },
        biography : {
            type : DataTypes.STRING(500),
            allowNull : true
        },
    }, {
        // Other model options go here
        sequelize: sequelizeConnection, // We need to choose the model name
        tableName: 'Authors', // We need to pass the connection instance
        timestamps: false,
        schema: 'public'
    });


export default Author;