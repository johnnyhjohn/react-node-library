import { Sequelize, DataTypes, Model } from "sequelize";
import sequelizeConnection from '../../../config/database'

import Author from "./Author";
import Category from "./Category";
import Publisher from "./Publisher";

class Book extends Model{}

Book.init({
        id : {
            type: DataTypes.UUID,
            defaultValue : DataTypes.UUIDV4,
            allowNull: false,
            primaryKey: true
        },
        title : {
            type: DataTypes.STRING( 255 ),
            allowNull : false
        },
        description : {
            type : DataTypes.STRING(500),
            allowNull : true
        },
        pages : {
            type : DataTypes.INTEGER,
            allowNull : false
        },
        language : {
            type : DataTypes.STRING(5),
            allowNull : true
        },
        id_author : {
            type : DataTypes.UUID,
            allowNull : false,
            references : {
                model : 'Authors',
                key : 'id'
            }
        },
        id_category : {
            type : DataTypes.UUID,
            allowNull : false,
            references : {
                model : 'Categories',
                key : 'id'
            }
        },
        id_publisher : {
            type : DataTypes.UUID,
            allowNull : false,
            references : {
                model : 'Publishers',
                key : 'id'
            }
        },
    }, {
        // Other model options go here
        sequelize: sequelizeConnection, // We need to choose the model name
        tableName: 'Books', // We need to pass the connection instance
        timestamps: false,
        schema: 'public'
});

Book.belongsTo(Author, { foreignKey: 'id_author', targetKey: 'id', as: Author.tableName })
Book.belongsTo(Category, { foreignKey: 'id_category', targetKey: 'id', as: 'Categories' })
Book.belongsTo(Publisher, { foreignKey: 'id_publisher', targetKey: 'id', as: 'Publishers' })


Author.hasMany(Book, {
    as: 'Books',
    foreignKey: 'id_author',
    sourceKey: 'id'
})

Category.hasMany(Book, {
    as: 'Books',
    foreignKey: 'id_category',
    sourceKey: 'id'
})

Publisher.hasMany(Book, {
    as: 'Books',
    foreignKey: 'id_publisher',
    sourceKey: 'id'
})


export default Book;