import { Sequelize, DataTypes, Model } from "sequelize";
import sequelizeConnection from '../../../config/database'

import Book from "./Book";
import User from "./User";


class BookReview extends Model{}

BookReview.init({
        id : {
            type: DataTypes.UUID,
            defaultValue : DataTypes.UUIDV4,
            allowNull: false,
            primaryKey: true
        },
        title : {
            type: DataTypes.STRING( 255 ),
            allowNull : false
        },
        review : {
            type : DataTypes.STRING(2000),
            allowNull : false
        },
        rating : {
            type : DataTypes.INTEGER,
            allowNull : false
        },
        id_user : {
            type : DataTypes.UUID,
            allowNull : false,
            references : {
                model : 'Users',
                key : 'id'
            }
        },
        id_book : {
            type : DataTypes.UUID,
            allowNull : false,
            references : {
                model : 'Books',
                key : 'id'
            }
        },
        },
     {
        // Other model options go here
        sequelize: sequelizeConnection, // We need to choose the model name
        tableName: 'BookReviews', // We need to pass the connection instance
        timestamps: false,
        schema: 'public'
});

BookReview.belongsTo(User, { foreignKey: 'id_user', targetKey: 'id', as: User.tableName })
BookReview.belongsTo(Book, { foreignKey: 'id_book', targetKey: 'id', as: Book.tableName })


export default BookReview;