import { Sequelize, DataTypes, Model } from "sequelize";
import sequelizeConnection from '../../../config/database'

class Publisher extends Model{}

Publisher.init({
        id : {
            type: DataTypes.UUID,
            defaultValue : DataTypes.UUIDV4,
            allowNull: false,
            primaryKey: true
        },
        name : {
            type: DataTypes.STRING( 255 ),
            allowNull : false
        },
        website : {
            type : DataTypes.STRING(500),
            allowNull : true
        },
    }, {
        // Other model options go here
        sequelize: sequelizeConnection, // We need to choose the model name
        tableName: 'Publishers', // We need to pass the connection instance
        timestamps: false,
        schema: 'public'
    });

export default Publisher;