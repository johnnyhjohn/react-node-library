import { Sequelize, DataTypes, Model, STRING } from "sequelize";
import sequelizeConnection from '../../../config/database'
import AuthToken from "./AuthToken";


class User extends Model{}

User.init({
    id : {
        type: DataTypes.UUID,
        defaultValue : DataTypes.UUIDV4,
        allowNull: false,
        primaryKey: true
    },
    username : {
        type: DataTypes.STRING( 255 ),
        allowNull : false
    },
    email : {
        type : DataTypes.STRING(255),
        allowNull : false
    },
    image : {
        type : DataTypes.STRING(600),
        allowNull : true
    },
    password : {
        type : DataTypes.STRING(255),
        allowNull : false 
    },
}, {
    // Other model options go here
    sequelize: sequelizeConnection, // We need to choose the model name
    tableName: 'Users', // We need to pass the connection instance
    timestamps: false,
    schema: 'public'
});

export default User;