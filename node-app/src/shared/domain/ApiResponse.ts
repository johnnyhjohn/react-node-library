import { ValidationStatus } from "src/app/domain/validation/validation.entity";

export interface IResponse {}
  
export class ApiResponse implements IResponse {
    public readonly message: string;
    public readonly data: any;
    public readonly status : ValidationStatus;
    public readonly error : string;
  
    constructor (record: any, message : string) {
        this.data = record;
        this.message = message;
    }
  
    returnError(error : string) : IResponse{
        return {
            message : this.message,
            status : ValidationStatus.ERROR,
            error : error
        }
    }

    returnSuccess() : IResponse {
        return {
            message : this.message,
            status : ValidationStatus.SUCCESS,
            data : this.data
        }        
    }
}