const isEntity = (v: any): v is Entity<any> => {
  return v instanceof Entity;
};

export interface IEntity {}

export abstract class Entity<T> implements IEntity {
  protected readonly _id: string;
  public readonly record: T;

  constructor (record: T) {
    this.record = record;
  }

  getRecord() {
    return this.record;
  }
  public equals (object?: Entity<T>) : boolean {

    if (object == null || object == undefined) {
      return false;
    }

    if (this === object) {
      return true;
    }

    if (!isEntity(object)) {
      return false;
    }
  }
}