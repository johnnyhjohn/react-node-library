import { ICollection } from "src/shared/collection/collection.entity";
import { IEntity } from "src/shared/domain/Entity";

export interface IQueryResult {
    toList : Array<IEntity>;
    toCollection : ICollection;
}