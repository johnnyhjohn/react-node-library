import { AuthToken } from "src/app/domain/auth/authToken";

export class AuthMiddleware {

    async validateRequest( requestContext : any ) : Promise<boolean>{

        if( this.isLoginOrRegisterRoute( requestContext ) ) return true;

        if( this.hasToken(requestContext) ) {
            return this.isValidToken(requestContext.headers['access_token']);
        }
    
        return false;
    }

    async isValidToken( access_token : string ) {
        const authToken = new AuthToken();
        authToken.token = access_token;
        const tokenObject = await authToken.getToken();
        return tokenObject !== null;
    }
    
    hasToken( requestContext : any ) {
        return requestContext.headers['access_token'] !== null 
            && requestContext.headers['access_token'] !== undefined
            && requestContext.headers['access_token'] !== ''
    }
    
    isLoginOrRegisterRoute( requestContext : any ) {
        return (requestContext.route.path == '/auth/login') || (requestContext.route.path == '/auth/register');
    }
}