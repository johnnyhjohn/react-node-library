import React, {useState, useEffect} from 'react';

import Snackbar from '@mui/material/Snackbar';
import MuiAlert from '@mui/material/Alert';

import EventBus from "../EventBus/EventBus";
import EVENTS from '../EventBus/Events';

const AlertMUI = React.forwardRef(function Alert(props, ref) {
    return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

function Alert() {

    const [state, setState] = useState({
        open: false,
        vertical: 'top',
        horizontal: 'center',
    });
    const [message, setMessage] = useState('')
    const [severity, setSeverity] = useState('')

    const { vertical, horizontal, open } = state;

    useEffect(() => {
        EventBus.on( EVENTS.ALERT, (messagePayload) => {
            setMessage((messagePayload.status === 'error') ? messagePayload.error : messagePayload.message);
            setSeverity(messagePayload.status);
            setState({...state, open:true});
        })
    }, []);

    const handleClose = () => {
        setState({...state, open:false});
    }

    return (
        <div>
            <Snackbar
                anchorOrigin={{ vertical, horizontal }}
                open={open}
                autoHideDuration={6000}
                key={vertical + horizontal}
            >
                <AlertMUI onClose={handleClose} severity={severity} sx={{ width: '100%' }}>
                    {message}
                </AlertMUI>
            </Snackbar>
        </div>);
}

export default React.memo(Alert);