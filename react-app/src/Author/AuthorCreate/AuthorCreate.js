import React, { useState, useContext } from 'react';
import UserContext from '../../context/UserContext';
import AuthorService from '../../services/AuthorService';

import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';

import EventBus from '../../EventBus/EventBus';
import EVENTS from '../../EventBus/Events';


function AuthorCreateComponent (props) {
    
    const [author, setAuthor] = useState({
        name : '',
        biography : ''
    });

    const userContext = useContext(UserContext)
    const authorService = new AuthorService(userContext.token);
    
    const handleFieldChange = (event, fieldName) => {
        setAuthor({ ...author, [fieldName] : event.target.value });
    }

    const createAuthor = async () => {
        const createRequest = await authorService.createAuthor( author );
        if( createRequest == null ) return;

        if( createRequest.status == 'success' ) {
            const authorListUpdated = props.authors.map(value => value);
            authorListUpdated.push(createRequest.data);
            props.setAuthors(authorListUpdated);
            setAuthor({name:'', bipgraphy: ''});
            props.setOpen(false);
        }

        EventBus.dispatch(EVENTS.ALERT, createRequest);
    }

    return(
        <React.Fragment>
            <Dialog open={props.open}>
                <DialogTitle>Create Author</DialogTitle>
                <DialogContent>
                    <TextField 
                        id="demo-helper-text-misaligned-no-helper"
                        autoFocus
                        fullWidth
                        variant="standard"
                        label="Name" 
                        value={author.name}
                        onChange={(e) => handleFieldChange(e, 'name')} />
                    <TextField 
                        id="demo-helper-text-misaligned-no-helper" 
                        variant="standard"
                        fullWidth
                        label="Biography" 
                        value={author.biography} 
                        onChange={(e) => handleFieldChange(e, 'biography')} />
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => props.setOpen(false)}>Cancel</Button>
                    <Button onClick={createAuthor} >CREATE </Button>
                </DialogActions>
            </Dialog>
        </React.Fragment>
    );
}

export default AuthorCreateComponent;