import React, {useState, useEffect, useContext} from "react";

import UserContext from '../context/UserContext';

import Paper from '@mui/material/Paper';
import {DataGrid} from '@mui/x-data-grid';

import EventBus from "../EventBus/EventBus";

import AuthorService from "../services/AuthorService";
import ListColumn from "./AuthorListColumns/ListColumns";
import EVENTS from "../EventBus/Events";

import AuthorCreateComponent from "./AuthorCreate/AuthorCreate";
import DeleteUpdateDialog from "../DeleteUpdateDialog/DeleteUpdateDialog";

import CustomNoRowsOverlay from "../modules/NoRowsOverlay/NoRowsOverlay";
import { useTheme } from '@mui/material/styles';

import Zoom from '@mui/material/Zoom';
import Fab from '@mui/material/Fab';
import AddIcon from '@mui/icons-material/Add';


function AuthorList() {
    const theme = useTheme();
    const userContext = useContext(UserContext)
    const authorService = new AuthorService(userContext.token);

    const [authors, setAuthors] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const [isCreateOpen, setIsCreateOpen] = useState(false);
    const [isOpenDialog, setIsOpenDialog] = useState(false);
    const [author, setAuthor] = useState({});
    const [dialogAction, setDialogAction] = useState('');

    const transitionDuration = {
        enter: theme.transitions.duration.enteringScreen,
        exit: theme.transitions.duration.leavingScreen,
    };

    EventBus.on( EVENTS.DELETE('author'), (authorToRemove) =>  {
        setDialogAction('delete');
        handleDialogProps(authorToRemove);
    })

    EventBus.on( EVENTS.EDIT('author'), (authorToUpdate) =>  {
        setDialogAction('update');
        handleDialogProps(authorToUpdate);
    })

    const handleDialogProps = (authorToHandle) => {
        setIsOpenDialog(true);
        setAuthor(authorToHandle);
    }

    useEffect( () => { 
        const handleAuthorsRequest = async () => {
            const authorsRequest = await authorService.getAuthors();
            setAuthors(authorsRequest);
            setIsLoading(false);
            return authorsRequest;
        }
        handleAuthorsRequest();
        EventBus.remove(EVENTS.DELETE('author'));
        EventBus.remove(EVENTS.EDIT('author'));
    }, []);


    return(<React.Fragment>
        <Paper elevation={2} sx={{padding:1, minHeight:200}}>
            <AuthorCreateComponent 
                open={isCreateOpen} 
                setOpen={setIsCreateOpen} 
                authors={authors} 
                setAuthors={setAuthors} />
            <DeleteUpdateDialog  
                open={isOpenDialog} 
                setOpen={setIsOpenDialog} 
                action={dialogAction}
                objectName='Author'
                object={author}
                objectList={authors}
                setObjectList={setAuthors}
                objectService={authorService}
            />
            { (authors.length > 0) 
                ?  <DataGrid
                    rows={authors}
                    columns={ListColumn}
                    pageSize={20}
                    autoHeight={true}
                    sx={{border:'none'}}
                    rowsPerPageOptions={[20]}
                    />
                : <DataGrid
                    rows={[]}
                    columns={ListColumn}
                    loading={isLoading}
                    sx={{border:'none',minHeight:250}}
                    components={{
                        NoRowsOverlay: CustomNoRowsOverlay,
                    }}
                    rowsPerPageOptions={[20]}
            /> }
        </Paper>
        <Zoom
            in={true}
            timeout={transitionDuration}
            style={{
                transitionDelay: `${transitionDuration.exit}ms`,
            }}
          unmountOnExit
        >
            <Fab className='btn-float' onClick={() => setIsCreateOpen( true )}>
                <AddIcon />
            </Fab>
        </Zoom>
    </React.Fragment>);
}


export default AuthorList;