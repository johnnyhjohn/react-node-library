import React, {useState, useEffect, useContext} from "react";
import BookService from '../services/BookService';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import { DataGrid } from '@mui/x-data-grid';

import UserContext from '../context/UserContext';

const columns = [
    { field: 'title', headerName: 'Title',flex: 1},
    { field: 'description', headerName: 'Description',flex: 1},
    { field: 'pages', headerName: 'Pages',flex: 1},
    { field: 'language', headerName: 'Language',flex: 1},
    { 
        field: 'category', headerName: 'Category',flex: 1,
        valueGetter: (params) => `${params.row.category?.name || ''}`
    },{ 
        field: 'author', headerName: 'Author',flex: 1,
        valueGetter: (params) => `${params.row.author?.name || ''}`
    }, { 
        field: 'publisher', 
        headerName: 'Publisher',flex: 1,
        valueGetter: (params) => `${params.row.publisher?.name || ''}`
    },
  ];

function BookList() {
    const userContext = useContext(UserContext);
    const [books, setBooks] = useState([]);
    
    useEffect(() => {
        const bookService = new BookService(userContext.token);
        const handleGetBooks = async function() {
            
            const booksRequest = await bookService.getBooks();
            setBooks(booksRequest);
        }
        handleGetBooks();
    }, []);

        return (<React.Fragment>
            <Paper elevation={2}>
                <h2 className="card-title">Books</h2>
                <div>
                    <div style={{ height: 400}}>

                    { (books.length > 0) 
                        ?  <DataGrid
                                rows={books}
                                columns={columns}
                                pageSize={20}
                                rowsPerPageOptions={[20]}
                                checkboxSelection
                            />
                        : <DataGrid
                                rows={[]}
                                columns={columns}
                                loading={true}
                                rowsPerPageOptions={[20]}
                            checkboxSelection
                        /> }
                    </div>
                </div>
            </Paper>
        </React.Fragment>);
}

export default React.memo(BookList);
