import React, { useState, useContext } from 'react';

import UserContext from '../../context/UserContext';
import CategoryService from '../../services/CategoryService';

import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';

import EventBus from '../../EventBus/EventBus';
import EVENTS from '../../EventBus/Events';


function CategoryCreateComponent (props) {
    
    const [category, setCategory] = useState({
        name : '',
        description : ''
    });

    const userContext = useContext(UserContext);
    const categoryService = new CategoryService(userContext.token);
    
    const handleFieldChange = (event, fieldName) => {
        setCategory({ ...category, [fieldName] : event.target.value });
    }

    const createCategory = async () => {
        const createRequest = await categoryService.createCategory( category );
        if( createRequest == null ) return;

        if( createRequest.status === 'success' ) {
            const categoryListUpdated = props.categories.map(value => value);
            categoryListUpdated.push(createRequest.data);
            props.setCategories(categoryListUpdated);
            setCategory({name:'', description: ''});
            props.setOpen(false);
        }

        EventBus.dispatch(EVENTS.ALERT, createRequest);
    }

    return(
        <React.Fragment>
            <Dialog open={props.open}>
                <DialogTitle>Create Category</DialogTitle>
                <DialogContent>
                    <TextField 
                        id="demo-helper-text-misaligned-no-helper"
                        autoFocus
                        fullWidth
                        variant="standard"
                        label="Name" 
                        value={category.name}
                        onChange={(e) => handleFieldChange(e, 'name')} />
                    <TextField 
                        id="demo-helper-text-misaligned-no-helper" 
                        variant="standard"
                        fullWidth
                        label="Description" 
                        value={category.description} 
                        onChange={(e) => handleFieldChange(e, 'description')} />
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => props.setOpen(false)}>Cancel</Button>
                    <Button onClick={createCategory} >CREATE </Button>
                </DialogActions>
            </Dialog>
        </React.Fragment>
    );
}

export default CategoryCreateComponent;