import React, {useState, useEffect, useContext} from "react";
import UserContext from "../context/UserContext";

import Paper from '@mui/material/Paper';
import {DataGrid} from '@mui/x-data-grid';

import EventBus from "../EventBus/EventBus";

import CategoryService from "../services/CategoryService";
import ListColumn from "./CategoryListColumns/ListColumns";
import EVENTS from "../EventBus/Events";

import CategoryCreateComponent from "./CategoryCreate/CategoryCreate";
import DeleteUpdateDialog from "../DeleteUpdateDialog/DeleteUpdateDialog";

import CustomNoRowsOverlay from "../modules/NoRowsOverlay/NoRowsOverlay";
import { useTheme } from '@mui/material/styles';

import Zoom from '@mui/material/Zoom';
import Fab from '@mui/material/Fab';
import AddIcon from '@mui/icons-material/Add';


function CategoryList() {
    const theme = useTheme();
    const userContext = useContext(UserContext);
    const categoryService = new CategoryService(userContext.token);

    const [categories, setCategories] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const [isCreateOpen, setIsCreateOpen] = useState(false);
    const [isOpenDialog, setIsOpenDialog] = useState(false);
    const [category, setCategory] = useState({});
    const [dialogAction, setDialogAction] = useState('');

    const transitionDuration = {
        enter: theme.transitions.duration.enteringScreen,
        exit: theme.transitions.duration.leavingScreen,
    };

    EventBus.on( EVENTS.DELETE('category'), (categoryToRemove) =>  {
        setDialogAction('delete');
        handleDialogProps(categoryToRemove);
    })

    EventBus.on( EVENTS.EDIT('category'), (categoryToUpdate) =>  {
        setDialogAction('update');
        handleDialogProps(categoryToUpdate);
    })

    const handleDialogProps = (categoryToHandle) => {
        setIsOpenDialog(true);
        setCategory(categoryToHandle);
    }

    useEffect( () => {
        const handleCategoriesRequest = async () => {
            const categoriesRequest = await categoryService.getCategories();
            setCategories(categoriesRequest);
            setIsLoading(false);
            return categoriesRequest;
        }
        handleCategoriesRequest();
        EventBus.remove(EVENTS.DELETE('category'));
        EventBus.remove(EVENTS.EDIT('category'));
    }, []);


    return(<React.Fragment>
        <Paper elevation={2} sx={{padding:1, minHeight:200}}>
            <CategoryCreateComponent 
                open={isCreateOpen} 
                setOpen={setIsCreateOpen} 
                categories={categories} 
                setCategories={setCategories} />
            <DeleteUpdateDialog  
                open={isOpenDialog} 
                setOpen={setIsOpenDialog} 
                action={dialogAction}
                objectName='Category'
                object={category}
                objectList={categories}
                setObjectList={setCategories}
                objectService={categoryService}
            />
            { (categories.length > 0) 
                ?  <DataGrid
                    rows={categories}
                    columns={ListColumn}
                    pageSize={20}
                    autoHeight={true}
                    sx={{border:'none'}}
                    rowsPerPageOptions={[20]}
                    />
                : <DataGrid
                    rows={[]}
                    columns={ListColumn}
                    loading={isLoading}
                    sx={{border:'none',minHeight:250}}
                    components={{
                        NoRowsOverlay: CustomNoRowsOverlay,
                    }}
                    rowsPerPageOptions={[20]}
            /> }
        </Paper>
        <Zoom
            in={true}
            timeout={transitionDuration}
            style={{
                transitionDelay: `${transitionDuration.exit}ms`,
            }}
          unmountOnExit
        >
            <Fab className='btn-float' onClick={() => setIsCreateOpen( true )}>
                <AddIcon />
            </Fab>
        </Zoom>
    </React.Fragment>);
}


export default React.memo(CategoryList);