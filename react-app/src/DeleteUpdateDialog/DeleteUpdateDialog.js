import React, {useContext} from "react";

import UserContext from "../context/UserContext";
import Button from '@mui/material/Button';

import EventBus from "../EventBus/EventBus";

import EVENTS from "../EventBus/Events";

import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';


const DeleteUpdateDialog = function (props) {

    const handleDelete = async ( objectToDelete ) => {
        const deleteRequest = await props.objectService.delete(objectToDelete.id);
                
        if( deleteRequest.status === 'success' ) {    
            const newCategoryList = props.objectList.filter( (category) => category.id != objectToDelete.id );
            props.setObjectList(newCategoryList);
            props.setOpen(false);
        }

        EventBus.dispatch(EVENTS.ALERT, deleteRequest);
    }

    const handleUpdate = async ( objectToUpdate ) => {
        const updateRequest = await props.objectService.update(objectToUpdate);
        console.log('updateRequest : ', updateRequest);
        if( updateRequest.status === 'success' ) {
            props.setOpen(false);
        }

        EventBus.dispatch(EVENTS.ALERT, updateRequest);
    }

    return(
        <React.Fragment>
            <Dialog open={props.open}>
                <DialogTitle sx={{textTransform:'capitalize'}}>{props.action} {props.objectName}</DialogTitle>
                <DialogContent>
                    Are you sure you want {props.action} this {props.objectName}?
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => props.setOpen(false)}>Cancel</Button>
                    <Button onClick={() => (props.action === 'delete') ? handleDelete(props.object) : handleUpdate(props.object)} > {props.action} </Button>
                </DialogActions>
            </Dialog>
        </React.Fragment>
    );
}

export default DeleteUpdateDialog;