const EVENTS = {
    DELETE : (objectName) => `delete_${objectName}`,
    EDIT : (objectName) => `edit_${objectName}`,
    CREATE : (objectName) => `create_${objectName}`,
    ALERT : 'open_alert',
    LOGOUT : 'logout'
}

export default EVENTS;
