import './Login.css';
import React, {useState} from "react";

import Box from '@mui/material/Box';

import UserService from '../services/UserService';

import LoginForm from './LoginForm/LoginForm';
import RegistrationForm from './Registration/RegistrationForm';

function Login(props) {

    const [showRegisterForm, setShowRegisterForm] = useState(false);
    const userService = new UserService();

    return (<React.Fragment>
        <Box className="login-content">
            <div className="book">
                <div className="pages">
                    <div className="page left-page">
                        <div className="page-content">
                            <figure className="book-image">
                                <img src="/open-book.png"/>
                            </figure>
                            <div className="lines"></div>
                            <div className="lines"></div>
                            <div className="lines"></div>
                            <div className="lines"></div>
                            <div className="lines"></div>
                            <div className="lines"></div>
                        </div>
                    </div>
                    <div className="page right-page">    
                        <div className="page-content">
                            <h2 className="title">BOOK MANAGEMENT TOOL</h2>  
                            {(showRegisterForm)
                                ? <div>
                                    <RegistrationForm
                                        setSessionUser={props.setSessionUser} 
                                        setToken={props.setToken}
                                        userService={userService} /> 

                                    <a onClick={(event) => {event.preventDefault();setShowRegisterForm(false)}}>Back to login</a>
                                    </div>
                                : <div>
                                    <LoginForm 
                                        setSessionUser={props.setSessionUser} 
                                        setToken={props.setToken} 
                                        userService={userService} />
                                    <a onClick={(event) => {event.preventDefault();setShowRegisterForm(true)}}>Don't have an account?</a>
                                    <figure className="book-image-second-part">
                                        <img src="/girl-reading-book.png"/>
                                    </figure>  
                                </div>
                            }                      
                        </div>
                    </div>
                </div>
            </div>
        </Box>
    </React.Fragment>)
}

export default Login;