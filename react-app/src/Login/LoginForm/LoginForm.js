import React, {useState, useEffect} from "react";

import Box from '@mui/material/Box';
import IconButton from '@mui/material/IconButton';
import Input from '@mui/material/Input';
import Button from '@mui/material/Button';
import LoadingButton from '@mui/lab/LoadingButton';
import InputAdornment from '@mui/material/InputAdornment';
import FormGroup from '@mui/material/FormGroup';
import FormControl from '@mui/material/FormControl';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import TextField from '@mui/material/TextField';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';

import EVENTS from "../../EventBus/Events";
import EventBus from "../../EventBus/EventBus";

function LoginForm(props) {

    const [values, setValues] = useState({
        username: '',
        password: '',
        showPassword: false,
    });

    const [rememberMe, setRememberMe] = useState(false);
    const [isLoading, setIsLoading] = useState(false);

	useEffect(() => {
        const rememberData = localStorage.getItem('rememberMe');
        if(rememberData != null) {
            const rememberObj = JSON.parse(rememberData);
            setValues({...values, username : rememberObj.username, password : rememberObj.password});
            setRememberMe(true);
        }
	}, [])

    const handleChange = (prop) => (event) => {
        setValues({ ...values, [prop]: event.target.value });
    };

    const handleClickShowPassword = () => {
        setValues({ ...values, showPassword: !values.showPassword });

    }

    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };

    const login = async () => {
        setIsLoading(true);
        const loginRequest = await props.userService.login(values);
        EventBus.dispatch(EVENTS.ALERT, loginRequest);
        if( loginRequest.status === "success" ) {
            localStorage.setItem('token', JSON.stringify(loginRequest.data.authToken));
            localStorage.setItem('user', JSON.stringify(loginRequest.data.user));
            handleRemember();
            
            setTimeout(() => {
                props.setSessionUser(loginRequest.data.user);
                props.setToken(loginRequest.data.authToken.token);
            }, 1500);
        } else {
            setIsLoading(false);
        }
    }

    const handleRemember = () => (rememberMe === true) 
        ? localStorage.setItem('rememberMe', JSON.stringify(values))
        : localStorage.removeItem('rememberMe');

    return (<React.Fragment>
        <Box className="login-form">   
            <FormControl sx={{ m: 1, width: '95%' }}>
                <TextField
                    variant="standard" 
                    type="text"
                    label="Username"
                    value={values.username}
                    onChange={handleChange('username')}
                />
            </FormControl>
            <FormControl sx={{ m: 1, width: '95%' }}>
            <Input
                id="standard-adornment-password"
                type={values.showPassword ? 'text' : 'password'}
                value={values.password}
                onChange={handleChange('password')}
                endAdornment={
                <InputAdornment position="end">
                    <IconButton
                    aria-label="toggle password visibility"
                    onClick={handleClickShowPassword}
                    onMouseDown={handleMouseDownPassword}
                    >
                    {values.showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                </InputAdornment>
                }
            />
            </FormControl>

            {isLoading 
                ? <LoadingButton className="btn-login loading" loading variant="outlined">Loading</LoadingButton>
                : <Button onClick={login} className="btn-login">LOGIN </Button>
            }
            <FormGroup>
                <FormControlLabel control={<Checkbox checked={rememberMe} onChange={() => setRememberMe(!rememberMe)} />} label="Remember me" />
            </FormGroup>    
        </Box>
    </React.Fragment>)
}

export default LoginForm;