import React, {useState, useEffect} from "react";

import Box from '@mui/material/Box';
import IconButton from '@mui/material/IconButton';
import Input from '@mui/material/Input';
import Button from '@mui/material/Button';
import LoadingButton from '@mui/lab/LoadingButton';
import InputAdornment from '@mui/material/InputAdornment';
import FormControl from '@mui/material/FormControl';
import TextField from '@mui/material/TextField';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';

import EVENTS from "../../EventBus/Events";
import EventBus from "../../EventBus/EventBus";

function RegistrationForm(props) {

    const [values, setValues] = useState({
        username: '',
        email : '',
        password: '',
        confirmPassword: '',
        showPassword: false,
    });

    const [passwordMatch, setPasswordMatch] = useState(true);
    const [isLoading, setIsLoading] = useState(false);

    const handleChange = (prop) => (event) => {
        setValues({ ...values, [prop]: event.target.value });
    };

    const handleClickShowPassword = () => {
        setValues({ ...values, showPassword: !values.showPassword });

    }

    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };

    const fieldsAreValid = () => {
        let message = '';
        if( !passwordMatch ) {
            message = "Passwords doesn't match.";
        }
        if(values.password.length < 6 ) {
            message = "Password must have at least 6 characters.";
        }
        if(values.username === '') {
            message = "Username can not be empty.";
        }
        if(values.email === '') {
            message = "Email can not be empty.";
        }

        if( message !== '' ) {
            EventBus.dispatch(EVENTS.ALERT, {error:message, status:'error'}); 
            setIsLoading(false);
            return false;
        }

        return true;
    }
    const register = async () => {
        setIsLoading(true);
        if(!fieldsAreValid()) return;
        const registerRequest = await props.userService.register(values);
        EventBus.dispatch(EVENTS.ALERT, registerRequest);
        
        if( registerRequest.status === "success" ) {
            localStorage.setItem('token', JSON.stringify(registerRequest.data.authToken));
            localStorage.setItem('user', JSON.stringify(registerRequest.data.user));
            
            setTimeout(() => {
                props.setSessionUser(registerRequest.data.user);
                props.setToken(registerRequest.data.authToken.token);
            }, 1500);
        } else {
            setIsLoading(false);
        }
    }

    const handleChangeAndValidatePassword = (prop) => (event) => {
        setPasswordMatch(event.target.value == values.password);
        setValues({ ...values, confirmPassword: event.target.value });
    }

    return (<React.Fragment>
        <Box className="login-form">   
            <FormControl sx={{ m: 1, width: '95%' }}>
                <TextField
                    variant="standard" 
                    type="text"
                    label="Username"
                    value={values.username}
                    onChange={handleChange('username')}
                />
            </FormControl>
            <FormControl sx={{ m: 1, width: '95%' }}>
                <TextField
                    variant="standard" 
                    type="email"
                    label="Email"
                    value={values.email}
                    onChange={handleChange('email')}
                />
            </FormControl>
            <FormControl sx={{ m: 1, width: '95%' }}>
            <Input
                id="password"
                placeholder="Password"
                type={values.showPassword ? 'text' : 'password'}
                value={values.password}
                color={(passwordMatch) ? 'success' : 'error'}
                error={!passwordMatch}
                onChange={handleChange('password')}
                endAdornment={
                <InputAdornment position="end">
                    <IconButton
                    aria-label="toggle password visibility"
                    onClick={handleClickShowPassword}
                    onMouseDown={handleMouseDownPassword}
                    >
                    {values.showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                </InputAdornment>
                }
            />
            </FormControl>
            <FormControl sx={{ m: 1, width: '95%' }}>
            <Input
                placeholder="Confirm Password"
                id="confirm-password"
                type={values.showPassword ? 'text' : 'password'}
                value={values.confirmPassword}
                color={(passwordMatch) ? 'success' : 'error'}
                error={!passwordMatch}
                onChange={handleChangeAndValidatePassword()}
                endAdornment={
                <InputAdornment position="end">
                    <IconButton
                    aria-label="toggle password visibility"
                    onClick={handleClickShowPassword}
                    onMouseDown={handleMouseDownPassword}
                    >
                    {values.showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                </InputAdornment>
                }
            />
            </FormControl>
            {isLoading 
                ? <LoadingButton className="btn-login loading" loading variant="outlined">Loading</LoadingButton>
                : <Button onClick={register} className="btn-login">REGISTER </Button>
            }  
        </Box>
    </React.Fragment>)
}

export default RegistrationForm;