import './App.css';
import React, {useState, useEffect} from "react";
import { Routes, Route, Link } from "react-router-dom";

import { styled, useTheme } from '@mui/material/styles';
import Header from '../modules/header/Header';
import Sidebar from '../modules/sidebar/Sidebar';

import Alert from '../Alert/Alert';

import Books from '../Books/BookList';
import CategoryList from '../Categories/CategoryList';
import AuthorList from '../Author/AuthorList';
import PublisherList from '../Publisher/PublisherList';

import Login from '../Login/Login';
import EventBus from '../EventBus/EventBus';
import EVENTS from '../EventBus/Events';
import UserContext from '../context/UserContext';

const drawerWidth = 240;


const Main = styled('main', { shouldForwardProp: (prop) => prop !== 'open' })(
	({ theme, open }) => ({
	  flexGrow: 1,
	  paddingLeft: 260,
	  paddingTop:80,
	  paddingRight:20,
	  transition: theme.transitions.create('margin', {
		easing: theme.transitions.easing.sharp,
		duration: theme.transitions.duration.leavingScreen,
	  }),
	  marginLeft: `-${drawerWidth}px`,
	  ...(open && {
		transition: theme.transitions.create('margin', {
		  easing: theme.transitions.easing.easeOut,
		  duration: theme.transitions.duration.enteringScreen,
		}),
		marginLeft: 0,
	  }),
	}),
);

function getSessionInfo(infoToGet) {
	const tokenString = localStorage.getItem(infoToGet);
	const userToken = JSON.parse(tokenString);
	return (infoToGet === 'token') ? userToken?.token : userToken;
}

function App() {

    const [open, setOpen] = useState(true);
	const [token, setToken] = useState(getSessionInfo('token'));
	const [sessionUser, setSessionUser] = useState(getSessionInfo('user'));
	const [headerTitle, setHeaderTitle] = useState('Dashboard');

	useEffect(() => {
		const componentLocation = window.location.pathname.split('/');

		if( componentLocation.length > 1 && componentLocation[1] !== '') {
			setHeaderTitle(componentLocation[1]);
		}
		
		EventBus.on(EVENTS.LOGOUT, () => {
			localStorage.removeItem('user');
			localStorage.removeItem('token');
			setToken(null);
			setSessionUser(null);
		});

	}, [token])

	if( token != null ){
		return (
			<UserContext.Provider value={{token,sessionUser}}>
				<div className="App">
					<Alert />
					<Sidebar open={open} setOpen={setOpen} setHeaderTitle={setHeaderTitle}/>
					<Header open={open} setOpen={setOpen} headerTitle={headerTitle} />
					
					<Main open={open}>
						<Routes>
							<Route path="/" element={<Books />} />
							<Route path="/authors" element={<AuthorList />} />
							<Route path="/categories" element={<CategoryList />} />
							<Route path="/publishers" element={<PublisherList />} />
						</Routes>
					</Main>
				</div>
			</UserContext.Provider>
		);
	} else {
		return (
			<div className="App">
				<Alert />
				<Login setToken={setToken} setSessionUser={setSessionUser}/>
			</div>
		)
	}
}

export default App;