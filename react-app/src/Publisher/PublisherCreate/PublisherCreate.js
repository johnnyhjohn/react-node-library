import React, { useState, useContext } from 'react';

import UserContext from '../../context/UserContext';
import PublisherService from '../../services/PublisherService';

import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';

import EventBus from '../../EventBus/EventBus';
import EVENTS from '../../EventBus/Events';


function PublisherCreateComponent (props) {
    
    const [publisher, setPublisher] = useState({
        name : '',
        website : ''
    });

    const userContext = useContext(UserContext);
    const publisherService = new PublisherService(userContext.token);
    
    const handleFieldChange = (event, fieldName) => {
        setPublisher({ ...publisher, [fieldName] : event.target.value });
    }

    const createPublisher = async () => {
        const createRequest = await publisherService.createPublisher( publisher );
        if( createRequest == null ) return;

        if( createRequest.status === 'success' ) {
            const publisherListUpdated = props.publishers.map(value => value);
            publisherListUpdated.push(createRequest.data);
            props.setPublishers(publisherListUpdated);
            setPublisher({name:'', website: ''});
            props.setOpen(false);
        }

        EventBus.dispatch(EVENTS.ALERT, createRequest);
    }

    return(
        <React.Fragment>
            <Dialog open={props.open}>
                <DialogTitle>Create Publisher</DialogTitle>
                <DialogContent>
                    <TextField 
                        id="demo-helper-text-misaligned-no-helper"
                        autoFocus
                        fullWidth
                        variant="standard"
                        label="Name" 
                        value={publisher.name}
                        onChange={(e) => handleFieldChange(e, 'name')} />
                    <TextField 
                        id="demo-helper-text-misaligned-no-helper" 
                        variant="standard"
                        fullWidth
                        label="Website" 
                        value={publisher.website} 
                        onChange={(e) => handleFieldChange(e, 'website')} />
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => props.setOpen(false)}>Cancel</Button>
                    <Button onClick={createPublisher} >CREATE </Button>
                </DialogActions>
            </Dialog>
        </React.Fragment>
    );
}

export default PublisherCreateComponent;