import React, {useState, useEffect, useContext} from "react";
import UserContext from "../context/UserContext";

import Paper from '@mui/material/Paper';
import {DataGrid} from '@mui/x-data-grid';

import EventBus from "../EventBus/EventBus";

import PublisherService from "../services/PublisherService";
import ListColumn from "./PublisherListColumns/ListColumns";
import EVENTS from "../EventBus/Events";

import PublisherCreateComponent from "./PublisherCreate/PublisherCreate";
import DeleteUpdateDialog from "../DeleteUpdateDialog/DeleteUpdateDialog";

import CustomNoRowsOverlay from "../modules/NoRowsOverlay/NoRowsOverlay";
import { useTheme } from '@mui/material/styles';

import Zoom from '@mui/material/Zoom';
import Fab from '@mui/material/Fab';
import AddIcon from '@mui/icons-material/Add';


function PublisherList() {
    const theme = useTheme();
    const userContext = useContext(UserContext);
    const publisherService = new PublisherService(userContext.token);

    const [publishers, setPublishers] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const [isCreateOpen, setIsCreateOpen] = useState(false);
    const [isOpenDialog, setIsOpenDialog] = useState(false);
    const [publisher, setPublisher] = useState({});
    const [dialogAction, setDialogAction] = useState('');

    const transitionDuration = {
        enter: theme.transitions.duration.enteringScreen,
        exit: theme.transitions.duration.leavingScreen,
    };

    EventBus.on( EVENTS.DELETE('publisher'), (publisherToRemove) =>  {
        setDialogAction('delete');
        handleDialogProps(publisherToRemove);
    })

    EventBus.on( EVENTS.EDIT('publisher'), (publisherToUpdate) =>  {
        setDialogAction('update');
        handleDialogProps(publisherToUpdate);
    })

    const handleDialogProps = (publisherToHandle) => {
        setIsOpenDialog(true);
        setPublisher(publisherToHandle);
    }

    useEffect( () => {
        const handlePublisherRequest = async () => {
            const publisherRequest = await publisherService.getPublishers();
            console.log('publisherRequest : ', publisherRequest);
            setPublishers(publisherRequest);
            setIsLoading(false);
            return publisherRequest;
        }
        handlePublisherRequest();
        EventBus.remove(EVENTS.DELETE('publisher'));
        EventBus.remove(EVENTS.EDIT('publisher'));
    }, []);


    return(<React.Fragment>
        <Paper elevation={2} sx={{padding:1, minHeight:200}}>
            <PublisherCreateComponent 
                open={isCreateOpen} 
                setOpen={setIsCreateOpen} 
                publishers={publishers} 
                setPublishers={setPublishers} />
            <DeleteUpdateDialog  
                open={isOpenDialog} 
                setOpen={setIsOpenDialog} 
                action={dialogAction}
                objectName='Publisher'
                object={publisher}
                objectList={publishers}
                setObjectList={setPublishers}
                objectService={publisherService}
            />
            { (publishers.length > 0) 
                ?  <DataGrid
                    rows={publishers}
                    columns={ListColumn}
                    pageSize={20}
                    autoHeight={true}
                    sx={{border:'none'}}
                    rowsPerPageOptions={[20]}
                    />
                : <DataGrid
                    rows={[]}
                    columns={ListColumn}
                    loading={isLoading}
                    sx={{border:'none',minHeight:250}}
                    components={{
                        NoRowsOverlay: CustomNoRowsOverlay,
                    }}
                    rowsPerPageOptions={[20]}
            /> }
        </Paper>
        <Zoom
            in={true}
            timeout={transitionDuration}
            style={{
                transitionDelay: `${transitionDuration.exit}ms`,
            }}
          unmountOnExit
        >
            <Fab className='btn-float' onClick={() => setIsCreateOpen( true )}>
                <AddIcon />
            </Fab>
        </Zoom>
    </React.Fragment>);
}


export default React.memo(PublisherList);