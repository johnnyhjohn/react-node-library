import BinIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';

import IconButton from '@mui/material/IconButton';
import Stack from '@mui/material/Stack';

import EventBus from '../../EventBus/EventBus';
import EVENTS from '../../EventBus/Events';
import { Link } from 'react-router-dom';

const ListColumn = [
    { field: 'name', headerName: 'Name', editable:true, flex: 1},
    { 
        field: 'website', 
        headerName: 'Website',
        editable:true, 
        flex: 1,
        renderCell: (params) => {
            return (<a target="_blank" href={params.row.website}>{params.row.website}</a>);
        }
    },
    {
        field: "action",
        headerName: "Actions",
        sortable: false,
        width: 130,
        disableClickEventBubbling: true,
        renderCell: (params) => {

            const handleDelete = () => {
                EventBus.dispatch(EVENTS.DELETE('publisher'), params.row);
            }

            const handleUpdate = () => {
                EventBus.dispatch(EVENTS.EDIT('publisher'), params.row);
            }

            return (
                <Stack direction="row" spacing={1}>
                    <IconButton aria-label="update" onClick={handleUpdate}>
                        <EditIcon />
                    </IconButton>
                    <IconButton aria-label="delete" onClick={handleDelete}>
                        <BinIcon />
                    </IconButton>
                </Stack>
            );
        }
      }
];

export default ListColumn;