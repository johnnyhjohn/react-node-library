import React, {useState, useEffect, useContext, createContext} from "react";

const UserContext = createContext('Unknown');

export default UserContext;