import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from "react-router-dom";
import { Provider } from 'react-redux'
import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import createSagaMiddleware from 'redux-saga';

import './index.css';
import App from './MainApp/App';
import reportWebVitals from './reportWebVitals';

let composeEnhancers = compose;

const rootReducer = combineReducers({
	// here the reducers
});
  
const sagaMiddleware = createSagaMiddleware();

const store = createStore(
	rootReducer,
	composeEnhancers(applyMiddleware(sagaMiddleware)),
);
  
const rootElement = document.getElementById('root')
ReactDOM.render(
  <Provider store={store}>
	  <BrowserRouter>
    		<App />
		</BrowserRouter>
  </Provider>,
  rootElement
)
// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
