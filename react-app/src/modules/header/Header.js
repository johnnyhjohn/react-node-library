import * as React from 'react';
import MuiAppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';

import IconButton from '@mui/material/IconButton';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';

import Typography from '@mui/material/Typography';
import MenuIcon from '@mui/icons-material/Menu';
import SearchAppBar from './searchbar/Searchbar';
import UserMenu from './userMenu/UserMenu';
import { styled, useTheme } from '@mui/material/styles';


import './Header.css';

const drawerWidth = 240;

const AppBar = styled(MuiAppBar, {
    shouldForwardProp: (prop) => prop !== 'open',
  })(({ theme, open }) => ({
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    ...(open && {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: `${drawerWidth}px`,
      transition: theme.transitions.create(['margin', 'width'], {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
    }),
  }));

export default function Header(props) {

    const handleDrawer = () => {
        props.setOpen(!props.open);
    };
    
    return (
        <Box sx={{ flexGrow: 1 }}>
            <AppBar position="fixed" className="App-header" open={props.open}>
                <Toolbar>
                <IconButton
                    size="large"
                    edge="start"
                    color="inherit"
                    aria-label="open drawer"
                    onClick={handleDrawer}
                    sx={{ mr: 2 }}
                >
                    {(props.open) ? <ChevronLeftIcon /> : <MenuIcon />}
                </IconButton>
                <Typography
                    variant="h6"
                    noWrap
                    className='header-title'
                    component="div"
                    sx={{ display: { xs: 'none', sm: 'block' } }}
                >
                    {props.headerTitle}
                </Typography>
                <SearchAppBar />
                <Box sx={{ flexGrow: 1 }} />
                <UserMenu />
                </Toolbar>
            </AppBar>
        </Box>
    );
}
