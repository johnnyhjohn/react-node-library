import React, { useState, useContext, useEffect  } from 'react';
import UserContext from '../../../context/UserContext';

import Box from '@mui/material/Box';
import IconButton from '@mui/material/IconButton';
import MenuItem from '@mui/material/MenuItem';
import Menu from '@mui/material/Menu';
import AccountCircle from '@mui/icons-material/AccountCircle';
import EventBus from '../../../EventBus/EventBus';
import EVENTS from '../../../EventBus/Events';
import UserService from '../../../services/UserService';


function UserMenu() {
    const [anchorEl, setAnchorEl] = useState(null);
    const [user, setUser] = useState(null);
    const isMenuOpen = Boolean(anchorEl);
    const userContext = useContext(UserContext);

    const handleProfileMenuOpen = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleMenuClose = () => {
        setAnchorEl(null);
    };

    const handleLogout = async () => {
        const logoutRequest = await new UserService().logout(userContext.token);
        EventBus.dispatch(EVENTS.ALERT, logoutRequest);
        if(logoutRequest.status === 'success') {
            setTimeout(() => {
                EventBus.dispatch(EVENTS.LOGOUT, {});
            }, 1500);
        }
    }

    useEffect( () => {
        const user = JSON.parse(localStorage.getItem('user'));
        setUser(user)
    }, []);

    const menuId = 'primary-search-account-menu';
    const renderMenu = (
        <Menu
        anchorEl={anchorEl}
        anchorOrigin={{
            vertical: 'top',
            horizontal: 'right',
        }}
        id={menuId}
        keepMounted
        transformOrigin={{
            vertical: 'top',
            horizontal: 'right',
        }}
        open={isMenuOpen}
        onClose={handleMenuClose}
        >
        <MenuItem onClick={handleMenuClose}>Profile</MenuItem>
        <MenuItem onClick={handleMenuClose}>My account</MenuItem>
        <MenuItem onClick={handleLogout}>Logout</MenuItem>
        </Menu>
    );

    return (
        <Box>
            <Box sx={{ display: { xs: 'none', md: 'flex' } }}>
                <IconButton
                size="large"
                edge="end"
                aria-label="account of current user"
                aria-controls={menuId}
                aria-haspopup="true"
                onClick={handleProfileMenuOpen}
                color="inherit"
                >
                <AccountCircle />
                </IconButton>
                </Box>
            {renderMenu}
        </Box>
    );
}

export default React.memo( UserMenu );