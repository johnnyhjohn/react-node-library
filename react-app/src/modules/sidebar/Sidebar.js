import * as React from 'react';
import { styled, useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import CssBaseline from '@mui/material/CssBaseline';
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';

import AccountBox from '@mui/icons-material/AccountBox';
import LibraryBooks from '@mui/icons-material/LibraryBooks';
import SupervisedUserCircleIcon from '@mui/icons-material/SupervisedUserCircle';
import Category from '@mui/icons-material/Category';

import { Routes, Route, Link } from "react-router-dom";

import UserContext from '../../context/UserContext';

import './Sidebar.css'

const drawerWidth = 240;

const DrawerHeader = styled('div')(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
}));

export default function Sidebar(props) {
  const theme = useTheme();
  const userContext = React.useContext(UserContext);
  
  return (
    <Box sx={{ display: 'flex' }}>
      <CssBaseline />
      <Drawer
            sx={{
            width: drawerWidth,
            flexShrink: 0,
            '& .MuiDrawer-paper': {
                width: drawerWidth,
                boxSizing: 'border-box',
            },
            }}
            variant="persistent"
            anchor="left"
            open={props.open}
        >
        <DrawerHeader>
            <h4>{userContext.sessionUser.username}</h4>
        </DrawerHeader>
        <Divider />
        <List>
            <ListItem button>
                <Link to="/" onClick={() => props.setHeaderTitle('Books')} className='sidebar-link'>
                    <ListItemIcon>
                        <LibraryBooks />
                    </ListItemIcon>
                    <ListItemText primary='Books' />
                </Link>
            </ListItem>
            <ListItem button>
                <Link to="/authors"onClick={() => props.setHeaderTitle('Authors')} className='sidebar-link'>
                    <ListItemIcon>
                        <AccountBox /> 
                    </ListItemIcon>
                    <ListItemText primary='Authors' />
                </Link>
            </ListItem>
            <ListItem button>
                <Link to="/categories" onClick={() => props.setHeaderTitle('Categories')} className='sidebar-link'>
                    <ListItemIcon>
                        <Category />
                    </ListItemIcon>
                    <ListItemText primary="Categories" />
                </Link>
            </ListItem>
            <ListItem button>
                <Link to="/publishers" onClick={() => props.setHeaderTitle('Publishers')} className='sidebar-link'>
                    <ListItemIcon>
                        <Category />
                    </ListItemIcon>
                    <ListItemText primary="Publishers" />
                </Link>
            </ListItem>
            <ListItem button>
                <Link to="/" onClick={() => props.setHeaderTitle('Users')} className='sidebar-link'>
                    <ListItemIcon>
                        <SupervisedUserCircleIcon /> 
                    </ListItemIcon>
                    <ListItemText primary='Users' />
                </Link>
            </ListItem>
        </List>
        <Divider />
      </Drawer>
    </Box>
  );
}
