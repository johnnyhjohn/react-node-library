import Service from './Service';

function AuthorService(accessToken) {

    this.getAuthors = async () => {
        return await new Service('author', accessToken).get();
    }

    this.createAuthor = async ( authorToInsert ) => {
        return await new Service('author', accessToken).post(authorToInsert);
    }

    this.update = async ( authorToUpdate ) => {
        return await new Service('author', accessToken).edit(authorToUpdate);
    }

    this.delete = async ( authorId ) => {
        return await new Service('author', accessToken).delete(authorId);
    }
}

export default AuthorService;
