import Service from './Service';

function BookService(accessToken) {

    this.getBooks = async () => {
        return await  new Service('books', accessToken).get();
    }

    this.createBook = async ( bookToInsert ) => {
        return await  new Service('books', accessToken).post(bookToInsert);
    }

    this.update = async ( bookToUpdate ) => {
        return await  new Service('books', accessToken).edit(bookToUpdate);
    }

    this.delete = async ( bookId ) => {
        return await  new Service('books', accessToken).delete(bookId);
    }
}

export default BookService;