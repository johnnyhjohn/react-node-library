import Service from './Service';

function CategoryService(accessToken) {
    
    this.getCategories = async () => {
        return await new Service('categories', accessToken).get();
    }

    this.createCategory = async ( categoryToInsert ) => {
        return await new Service('categories', accessToken).post(categoryToInsert);
    }

    this.update = async ( categoryToUpdate ) => {
        return await new Service('categories', accessToken).edit(categoryToUpdate);
    }

    this.delete = async ( categoryId ) => {
        return await new Service('categories', accessToken).delete(categoryId);
    }
}

export default CategoryService;