import Service from './Service';

function PublisherService(accessToken) {
    
    this.getPublishers = async () => {
        return await new Service('publishers', accessToken).get();
    }

    this.createPublisher = async ( categoryToInsert ) => {
        return await new Service('publishers', accessToken).post(categoryToInsert);
    }

    this.update = async ( categoryToUpdate ) => {
        return await new Service('publishers', accessToken).edit(categoryToUpdate);
    }

    this.delete = async ( categoryId ) => {
        return await new Service('publishers', accessToken).delete(categoryId);
    }
}

export default PublisherService;