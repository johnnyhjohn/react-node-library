import axios from 'axios';
import API_ENDPOINTS from './config';

function Service(objectName, userToken) {

    const API_SERVICE  = new API_ENDPOINTS(objectName);
    
    const config = {
        headers: {
            access_token: userToken,
        }
    }

    this.get = async () => {
        const getRequest = await axios.get(API_SERVICE.default, config);
        if( getRequest.data.status == 'success' ) {
            return getRequest.data.data;
        }
    }
    
    this.post = async ( objectToInsert ) => {
        const insertRequest = await axios.post(API_SERVICE.default, objectToInsert, config);
        return insertRequest.data
    }

    this.edit = async ( objectToUpdate ) => {
        const deleteRequest = await axios.put(API_SERVICE.withId(objectToUpdate.id), objectToUpdate, config);
        return deleteRequest.data
    }

    this.delete = async ( objectId ) => {
        const deleteRequest = await axios.delete(API_SERVICE.withId(objectId), config);
        return deleteRequest.data
    }
}

export default Service;