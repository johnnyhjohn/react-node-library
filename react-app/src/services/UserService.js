import axios from 'axios';
import API_ENDPOINTS from './config';

function UserService() {

    const API_SERVICE  = new API_ENDPOINTS('users');
    
    this.login = async ( userToAuthenticate ) => {
        const loginRequest = await axios.post(API_SERVICE.auth('login'), userToAuthenticate);
        return loginRequest.data
    }

    this.register = async ( userToRegister ) => {
        const registerRequest = await axios.post(API_SERVICE.auth('register'), userToRegister);
        return registerRequest.data
    }

    this.logout = async ( tokenToLogout ) => {
        const config = {
            headers: {
                access_token: tokenToLogout,
            }
        }
        const logoutRequest = await axios.post(API_SERVICE.auth('logout'), tokenToLogout, config);
        return logoutRequest.data
    }
}

export default UserService;