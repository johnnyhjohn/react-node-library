const CONFIG = {
    endpoint : 'http://localhost:3005/'
}

const API_ENDPOINTS = function(serviceName) {
    this.default = CONFIG.endpoint + serviceName;
    this.withId = (id) =>  `${CONFIG.endpoint}${serviceName}/${id}`;
    this.auth = (type) => `${CONFIG.endpoint}auth/${type}`;
    return this;
}

export default API_ENDPOINTS;